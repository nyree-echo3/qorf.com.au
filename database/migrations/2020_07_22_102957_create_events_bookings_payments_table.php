<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsBookingsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_bookings_payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('booking_id');
            $table->decimal('amount', 8, 2);            
            $table->enum('payment_method', ['paypal','bank-deposit']);
            $table->enum('payment_status', ['pending','completed'])->default('pending');
            $table->string('paypal_token')->nullable();
            $table->string('paypal_payer_id')->nullable();
            $table->string('payment_transaction_number')->nullable();
			$table->enum('is_processed', ['true','false'])->default('false');
            $table->text('note')->nullable();
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_bookings_payments');
    }
}
