<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('member_id');
            $table->decimal('amount', 8, 2);
            $table->enum('payment_type', ['initial','renew'])->default('initial');
            $table->enum('payment_method', ['paypal','bank-deposit']);
            $table->enum('payment_status', ['pending','completed'])->default('pending');
            $table->string('paypal_token')->nullable();
            $table->string('paypal_payer_id')->nullable();
            $table->string('payment_transaction_number')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_table');
    }
}
