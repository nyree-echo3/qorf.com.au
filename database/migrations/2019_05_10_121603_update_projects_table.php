<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['slug','meta_title', 'meta_description', 'meta_keywords', 'description', 'short_description', 'feature']);
        });
		
		Schema::table('projects', function (Blueprint $table) {
            $table->text('name')->nullable()->after('title');
			$table->text('summary')->nullable()->after('title');
			$table->string('recruitment_status')->nullable()->after('title');
			$table->text('eligible_cohort')->nullable()->after('title');
			$table->string('phase')->nullable()->after('title');
			$table->string('recruitment_target')->after('title');
			$table->string('expected_completion')->nullable()->after('title');
			$table->text('participating_sites')->nullable()->after('title');
			$table->text('collaborators')->nullable()->after('title');
			$table->text('co_sponsors')->nullable()->after('title');
			$table->text('link')->nullable()->after('title');
			$table->text('publications')->nullable()->after('title');
			$table->text('country_pi')->nullable()->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
