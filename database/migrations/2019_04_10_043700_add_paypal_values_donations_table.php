<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaypalValuesDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donations', function (Blueprint $table) {
            $table->dropColumn('payment_type');
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->enum('payment_type', ['once','regular'])->default('once')->after('amount');
            $table->enum('payment_method', ['bank-deposit','paypal'])->default('paypal')->after('payment_type');
            $table->string('paypal_token')->nullable()->after('payment_status');
            $table->string('paypal_payer_id')->nullable()->after('paypal_token');
            $table->string('paypal_profile_id')->nullable()->after('paypal_payer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donations', function (Blueprint $table) {
            //
        });
    }
}
