<?php
// Set Meta Tags
$meta_title_inner = "Make a Donation" . $company_name;
$meta_keywords_inner = "donation, " . $company_name;
$meta_description_inner = "Make a Donation | " . $company_name;

?>
@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

<style>
input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
</style>
   
    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-donation')       
                
                <div class="col-sm-9 blog-main">

                    <div class="blog-post">
                        <h1>Make a Donation</h1>
                        <blockquote><p>Thanks to your generosity - we can improve the lives of Queenslanders!</p></blockquote>
                        <p>Your generous donations will support our mission to improve the lives of Queenslanders by funding innovative orthopaedic research.  All costs for administering the Queensland Orthopaedic Research Fund are provided by the Australian Orthopaedic Association so that 100% of your donations can support this vital research.</p>
                        <p><a href="{{ url('') }}/members-portal/renew">Get involved by donating your services as a mentor or research professional -></a> </p>


                        <form id="donation-form" class="frmDonation" method="post"
                              action="{{ url('donations/save-donation') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="pages-donation">
                                <div class="mj_tab" id="tab-donation">
                                    <div class="mj_tab_items">

                                        <div class="active" data-tab="#tab01">Give Once</div>
                                        <div data-tab="#tab1">Give Regularly</div>                                        
                                    </div>

                                    <div class="mj_tab_contents">
                                        <div class="active" id="tab0">
                                            <h2>Give Once</h2>

                                            <div class="donationWrapper">
                                                <div class="container">
                                                    <section class="radSectionAmount">
                                                        <div class="row">
                                                            @php
                                                                $donationSelected = 0;
                                                                $iconCounter = 0;
                                                            @endphp

                                                            @foreach ($categories as $category)
                                                                @if ($category->type == "once")
                                                                    <div class="col-lg-4">
                                                                        <div class="donation-icon" id="donationIcon{{ $category->id }}">
																		   @if ($iconCounter == 0)                                                                  
																			  <i class="fas fa-user-md"></i>
																		   @elseif ($iconCounter == 1)   
																			  <i class="fas fa-diagnoses"></i>                                                                               
																		   @else
																			  <i class="fas fa-microscope"></i>
																		   @endif
																		</div>
                                                                       
                                                                        <input type="radio" name="radAmount"
                                                                               id="{{ $category->id }}"
                                                                               value="O{{ $category->amount }}"
                                                                               class="rad-donation cssAmount{{ $category->id }}" {{ (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount ? 'checked' : '') }}>

                                                                        <label for="{{ $category->id }}"
                                                                               class="label-amount donation-title">                                                                            
                                                                            ${{ number_format($category->amount, 0) }}                                                                        
                                                                        </label>
                                                                        
                                                                        <div class="donation-category-txt">{{ $category->title }}</div>                                                                             
                                                                    </div>

                                                                    @php
                                                                        if (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount)  {
                                                                            $donationSelected = 1;
                                                                        }
                                                                        
                                                                        $iconCounter++;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </section>

                                                </div>
                                            </div>

                                            <div class="donationWrapper donationOther">
                                                <div class="container">
                                                    <section class="radSectionAmount">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="radio" id="radAmountOtherOnce"
                                                                       name="radAmount" id="{{ $category->id }}"
                                                                       value="{{ $category->amount }}"
                                                                       class="" {{ (substr($donation, 1) > 0 && $donationSelected == 0 ? 'checked' : '') }}>
                                                                <div class="other-amt-txt">Any Amount $</div>
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="text" name="amountOtherOnce"
                                                                       class='txtDonationAmt'
                                                                       value='{{ (substr($donation, 1) > 0 && $donationSelected == 0 ? substr($donation, 1) : '') }}'>
                                                                <div class="fv-plugins-message-container" id="errorAmountOther"></div>
                                                            </div>

                                                            <div class="col-lg-4">
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>

                                        </div> <!-- END tab0 -->
                                        
                                        <div id="tab1">
                                            <h2>Give Regularly</h2>

                                            <div class="donationWrapper">
                                                <div class="container">
                                                    <section class="radSectionAmount">
                                                        <div class="row">
                                                            @php
                                                                $donationSelected = 0;
                                                                $iconCounter = 0;
                                                            @endphp
                                                            
                                                            @foreach ($categories as $category)
                                                                @if ($category->type == "regular")
                                                                    <div class="col-lg-4">
																		<div class="donation-icon" id="donationIcon{{ $category->id }}">
																		   @if ($iconCounter == 0)                                                                  
																			  <i class="fas fa-prescription-bottle-alt"></i>
																		   @elseif ($iconCounter == 1)   
																			  <i class="fas fa-vials"></i>                                                                          
																		   @else
																			  <i class="fas fa-id-card-alt"></i>  
																		   @endif
																		</div>                                                                                                                                                
                                                                        
                                                                        <input type="radio" name="radAmount"
                                                                               id="{{ $category->id }}"
                                                                               value="R{{ $category->amount }}"
                                                                               class="rad-donation" {{ (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount ? 'checked' : '') }}>
                                                                                                                                                                                                                                                                                       
                                                                        <label for="{{ $category->id }}"  class="label-amount donation-title label-amount-mth">                  
                                                                            ${{ number_format($category->amount, 0) }}                  
                                                                        </label>
                                                                        
                                                                        <div class="donation-category-txt">{{ $category->title }}</div>
                                                                        <div class="donation-per-month-txt">per month</div>                                                                        
                                                                    </div>

                                                                    @php
                                                                        if (substr($donation, 1) > 0 && substr($donation, 1) == $category->amount)  {
                                                                            $donationSelected = 1;
                                                                        }
                                                                        
                                                                        $iconCounter++;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </section>

                                                </div>
                                            </div>

                                            <div class="donationWrapper donationOther">
                                                <div class="container">
                                                    <section class="radSectionAmount">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <input type="radio" id="radAmountOtherRegular"
                                                                       name="radAmount" id="{{ $category->id }}"
                                                                       value="{{ $category->amount }}"
                                                                       class="" {{ (substr($donation, 1) > 0 && $donationSelected == 0 ? 'checked' : '') }}>
                                                                <div class="other-amt-txt">Any Amount $</div>
                                                            </div>

                                                            <div class="col-lg-4">
                                                                <input type="text" name="amountOtherRegular"
                                                                       class='txtDonationAmt'
                                                                       value='{{ (substr($donation, 1) > 0 && $donationSelected == 0 ? substr($donation, 1) : '') }}'>
                                                                <div class="fv-plugins-message-container"
                                                                     id="errorAmountOtherRegular"></div>
                                                            </div>

                                                            <div class="col-lg-4">
                                                               <div class="donation-per-month-txt2">PER MONTH</div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>

                                        </div> <!-- END tab1 -->
                                        

                                    </div> <!-- END mj_tab_contents -->
                                </div> <!-- END tab-donation -->
                            </div> <!-- END pages-donation -->

                            <div class="donationWrapper {{ (substr($donation, 1) > 0 ? 'donationWrapperInvisible' : '') }}">
                                <div class="btnDonation">
                                    <button id="btnDonation" type="button" class="btn-donation">Donate Now</button>
                                </div>
                            </div>

                            @include('flash::message')
                            <div class="donationForm {{ (substr($donation, 1) > 0 ? 'donationFormVisible' : '') }}">
                                <div id="donation-form-fields"></div>

                                <div class="paymentDetails">
                                    <h2>Your Donation</h2>
                                    <div class="divDonationDetails">
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-help-block">Please select your donation options above</div>
                                        </div>
                                    </div>

                                    <h2>Your Payment</h2>

                                    <div class="paymentDetails-option">
                                        <input type="radio" id="radPaymentMethod" name="radPaymentMethod" value="paypal"
                                               checked>
                                        <img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal">
                                    </div>

                                    <div class="paymentDetails-option">
                                        <input type="radio" id="radPaymentMethod" name="radPaymentMethod"
                                               value="bank-deposit">
                                        <strong>Bank Deposit</strong>
                                        <div class="direct-deposit">
                                            {!!  $payment_direct_deposit !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-12 col-sm-10 g-recaptcha-container">
                                        <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <button type="submit" class="btn-donation">Submit Donation
                                </button>
                            </div>

                        </form>                       

                    </div>
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
    
    @include('site/partials/panel-donations')
    @include('site/partials/index-links')
@endsection


@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            $('#donation-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function (message) {

                        FormValidation.formValidation(
                            document.getElementById('donation-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });


            $("#btnDonation").click(function () {
                $(".donationForm").show();
                $("#btnDonation").hide();
            });

            $(".rad-donation").click(function () {				
				$('.donation-icon').css('color', '#f26a31');
				strTypeId = $('input[name = "radAmount"]:checked').attr("id");
				$('#donationIcon' + strTypeId).css('color', '#fff');			
				
                strTypeAmount = $('input[name = "radAmount"]:checked').val();
                strAmount = strTypeAmount.substring(1);
                strType = strTypeAmount.substring(0, 1);

                $('input[name = "donation"]').val(("$" + strAmount));

                if (strType == "R") {
                    $('input[name = "type"]').val(('regular'));
                    $('.divDonationDetails').html("You are giving a regular donation of $" + strAmount + ".");
                } else {
                    $('input[name = "type"]').val(('once'));
                    $('.divDonationDetails').html("You are giving a once only donation of $" + strAmount + ".");
                }

                $('input[name = "amountOtherRegular"]').val('');
                $('input[name = "amountOtherOnce"]').val('');
                $('#errorAmountOther').html('');
            });

            $('input[name = "amountOtherRegular"]').focusout(function () {

                if (isNaN($('input[name = "amountOtherRegular"]').val())) {

                    $('input[name = "amountOtherRegular"]').val('');
                    $('input[name = "donation"]').val('');

                    $('#errorAmountOtherRegular').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $2.00.</div>');
                } else if ($('input[name = "amountOtherRegular"]').val() < 2) {
                    $('input[name = "amountOtherRegular"]').val('');
                    $('input[name = "donation"]').val('');

                    $('#errorAmountOtherRegular').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $2.00.</div>');
                } else {

                    strAmount = parseFloat($('input[name = "amountOtherRegular"]').val()).toFixed(2);

                    $('input[name = "donation"]').val("$" + strAmount);
                    $('input[name = "amountOtherRegular"]').val(strAmount);
                    $('input[name = "amountOtherOnce"]').val('');
                    $('input[name = "type"]').val(('regular'));
                    $('.divDonationDetails').html("You are giving a regular donation of $" + strAmount + ".");

                    $('#errorAmountOtherRegular').html('');
                }
            });

            $('input[name = "amountOtherRegular"]').focus(function () {
                $('#radAmountOtherRegular').prop("checked", true);
            });

            $('input[name = "amountOtherOnce"]').focusout(function () {
                if (isNaN($('input[name = "amountOtherOnce"]').val())) {

                    $('input[name = "amountOtherOnce"]').val('');
                    $('input[name = "donation"]').val('');

                    $('#errorAmountOther').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $2.00.</div>');
                } else if ($('input[name = "amountOtherOnce"]').val() < 2) {
                    $('input[name = "amountOtherOnce"]').val('');
                    $('input[name = "donation"]').val('');

                    $('#errorAmountOther').html('<div class="fv-help-block" data-field="name" data-validator="notEmpty">The field is not valid.  It must be numeric and greater than $2.00.</div>');
                } else {

                    strAmount = parseFloat($('input[name = "amountOtherOnce"]').val()).toFixed(2);

                    $('input[name = "donation"]').val("$" + strAmount);
                    $('input[name = "amountOtherOnce"]').val(strAmount);
                    $('input[name = "amountOtherRegular"]').val('');
                    $('input[name = "type"]').val(('once'));
                    $('.divDonationDetails').html("You are giving a once only donation of $" + strAmount + ".");
                    $('#errorAmountOther').html('');
                }
            });

            $('input[name = "amountOtherOnce"]').focus(function () {
                $('#radAmountOtherOnce').prop("checked", true);
            });
			
			//$(".donation-icon").click(function(){			
			//	strId = this.id;
			//	alert(strId);
			//	jQuery("#6").attr('checked', true);
			
	//	});

        });
		
		

    </script>
@endsection		
