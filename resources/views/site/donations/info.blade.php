<?php 
   // Set Meta Tags
   $meta_title_inner = "Make a Donation" . $company_name; 
   $meta_keywords_inner = "donation, " . $company_name; 
   $meta_description_inner = "Make a Donation | " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                                   
        @include('site/partials/sidebar-donation')       
                
        <div class="col-sm-9 blog-main">

          <div class="blog-post">   
               <h1>Make a Donation</h1>
               <blockquote><p>Thanks to your generosity - we can improve the lives of Queenslanders!</p></blockquote>  
               @include('flash::message')           
          </div>
                      
            
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>

@include('site/partials/panel-donations')
@include('site/partials/index-links')

@endsection            
