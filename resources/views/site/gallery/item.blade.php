@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection

<div class="blog-masthead">         
    <div class="container">
      <div class="row">
        @include('site/partials/sidebar-gallery')        
        
        <div class="col-sm-9 blog-main">
          <div class="blog-post row">
          
			  <div class="col-12"><h1>{{ $gallery_category->name }}</h1></div>

			  @if ($gallery_category->description != "")
				 <div class="col-12">
					{!! $gallery_category->description !!}
				 </div> 
			  @endif

			  @if (isset($items)) 
			     <section class="gallery-baguetteBox">	
			         <div class="container-fluid">
					    <div class="row">	   	           
							 @foreach ($items as $item)	
								 <div class="col-lg-4 gallery-item">
									<a class="lightbox" href="{{ url('') }}{{$item->location}}" data-caption="{{$item->name}}<br>{!! $item->description !!}">			
									   <div class="gallery-a">
										 <div class="div-img">
											<img src="{{ url('') }}{{$item->location}}" alt="{{$item->name}}">
										 </div>
										 <div class="gallery-txt">
											<div class="gallery-name">{{ $item->name }}</div>
											<div class="gallery-description">{{ $item->description }}</div>											
										 </div>   
									   </div>							
									 </a>     			
								</div>                                                                 
							 @endforeach
						 </div>   
					 </div>   
			     </section>	 	        		 	  
			   @endif
         
			  </div><!-- /.blog-post -->                                                                             
         </div><!-- /.row -->
      </div><!-- /.blog-main -->
    </div><!-- /.container -->
 </div><!-- /.blog-masthead -->
@endsection

@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection

@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.gallery-baguetteBox', { animation: 'slideIn'});
        });
    </script>			
@endsection