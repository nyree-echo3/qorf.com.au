@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">                           
                @include('site/partials/sidebar-gallery')                

                <div class="col-sm-9 blog-main">

                    <div class="blog-post row">
                        <div class="col-12">   
                           <h1>Gallery</h1>                        
						</div>
                       
                        @if($items)                                                
                            @foreach($items as $item)                                                                
								<div class="col-lg-4 gallery-item">
								  <a href='{{ url('').'/'.$item->url }}'>				
									   <div class="gallery-a">
										 <div class="div-img">
											<img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}">
										 </div>
										 <div class="gallery-txt">
											<div class="gallery-name">{{ $item->name }}</div>
											<div class="gallery-description">{{ $item->description }}</div>											
										 </div>   
									   </div>							
								   </a>     			
								</div>                              
                            @endforeach						   
                        @else
                            <p>Currently there is no galleries to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>            
@endsection
