<!-- Header -->
<div class="header">
    <div class="header-logo">
        <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
        
    </div>  
    
    @include('site/partials/navigationV2')                                     
</div>