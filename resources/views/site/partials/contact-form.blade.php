@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

<div class="{{ ($page->slug == "mentoring-program" ? "page-form" :"contact-page") }}" >    
	<form id="contact-form" method="post" action="{{ url('contact/save-message') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<h2>Get Involved</h2>    
		
		<div id="contact-form-fields"></div>

		<div class="form-row">
			<div class="col-12 col-sm-10 g-recaptcha-container">
				<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
				@if ($errors->has('g-recaptcha-response'))
					<div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
				@endif
			</div>
		</div>
		<button type="submit" class="btn-checkout">Send</button>
	</form>
</div>

@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('inline-scripts-contact-form')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#contact-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('contact-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection