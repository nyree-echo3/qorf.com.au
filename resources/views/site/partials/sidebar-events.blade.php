<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Events</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @if($side_nav_mode=='manual')
                {!! $side_nav !!}
            @endif
            @if($side_nav_mode=='auto')
                @foreach ($side_nav as $item)
                    <li class='list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
                @endforeach
            @endif
        </ol>       
    </div>
</div>