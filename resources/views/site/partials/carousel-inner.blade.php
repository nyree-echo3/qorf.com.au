<div id="myCarousel" class="carousel slide" data-ride="carousel">
  @include('site/partials/helper-donation')            
  @include('site/partials/helper-members')
  
  <div class="carousel-inner inside-page">           
			<div class="carousel-item active">
		  	
		      @php
	            $imgUrl = "images/site/header-temp.jpg";
	            $imgAlt = "";
	            
	            // Page Module Category
		        if (isset($category) && sizeof($category) > 0)  {
	               if (!isset($page) || $page->header == "")  {
					   $imgUrl = $category[0]->header;
					   $imgAlt = $category[0]->name;
		           } else {
		               $imgUrl = $page->header;
					   $imgAlt = $page->title;
		           }
		        }
		      
		        // Module
		        if (isset($module))  {	         
		           $imgUrl = $module->header_image;
		           $imgAlt = $module->display_name;		           		         
		        } elseif  (isset($module) ) {
	               foreach ($side_nav as $item)  {
		              if ($item->slug == $category_slug)  {
		              $imgUrl = $item->header;
		              $imgAlt = $item->name;
		              }
		           }
		        }
		        	 	        	        
		      @endphp
		      
			  <img class="slide" src="{{ url('') }}/{{ $imgUrl }}" alt="{{ $imgAlt }}">		
			</div>
       
  </div>
</div>