<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Donate Now</h4>
        
        <div class="donate-charity">
           <img src="{{ url('') }}/images/site/ACNC-Registered-Charity-Logo_RGB.png" title="ACNC Registered Charity Logo" alt="ACNC Registered Charity Logo">
        </div>
        
        <ol class="navsidebar list-unstyled list-group-flush">
            @php
                if($side_nav_mode=='manual'){
                    echo $side_nav;
                }

                if($side_nav_mode=='auto'){

                    $count=0;

                    foreach($side_nav as $item){

                    $href = request()->path();

                    $style = '';
                    if($count==0 && $href=='documents'){
                        $style = 'active ';
                    }else if($href==$item->url){
                        $style = 'active ';
                    }
            @endphp
            <li class='{{ $style }}list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
            @php
                }
                $count++;
            }
            @endphp
        </ol>
    </div>
</div>