<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>News</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @if($side_nav_mode=='manual')
                {!! $side_nav !!}
            @endif
            @if($side_nav_mode=='auto')
                @foreach ($side_nav as $item)                                                       
                   <li class='list-group-item {{ (isset($current_category) && $current_category == $item->id ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>                    
                @endforeach
            @endif
        </ol>

        <!-- <div class='btn-back'>
            <a class='btn-home-news' href='{{ url('') }}/news/archive'>ARCHIVED NEWS <i class='fa fa-chevron-right'></i></a>
        </div>-->
    </div>
</div>