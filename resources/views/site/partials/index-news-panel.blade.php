 <div class="home-news-panel">   
   <div class="container-fluid p-0">
	  <div class="row">         		 	 
		  <div class="col-lg-6 home-news">
	         @include('site/partials/index-news')
		  </div><!-- /.col-lg-6 -->		
		  
		  <div class="col-lg-6 home-newsletter">
	         @include('site/partials/index-newsletter')
		  </div><!-- /.col-lg-6 -->		
		</div>
   </div>
</div>
