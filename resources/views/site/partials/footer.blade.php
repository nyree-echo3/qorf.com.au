<footer class='footer'>
 <div class='footerContentWrapper'>
	 <div class='footerContent'>
        <div class="panelNav">
	    <div class="container-fluid">
		<div class="row">
       
           <div class="col-lg-4">
		      <div class="center-block">
			     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-rev.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>			     
			  </div>			  			 
		   </div>  
                	              		   		   
		   
		   <div class="col-lg-4">
			   <div class="center-block footer-address">
			   {!! $contact_details !!}	
			   </div>
		   </div>
		   
		   <div class="col-lg-4">
			   <div class="center-block footer-phone">
			   @if ( $phone_number != "") <span><strong>T</strong> <a href="tel:{{ str_replace(' ', '', $phone_number) }}">{{ $phone_number }}</a></span><br> @endif 
			   @if ( $email != "") <strong>E</strong> <a href="mailto:{{ $email }}">{{ $email }}</a><br> @endif 
		   </div>
		  
		</div>				
  
	 </div> 	 	 		
 </div> 
	
 </div>
 
	
  </div>
  
    <div id="footer-txt"> 
		@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} All rights reserved</a> |@endif 		
		<a href="{{ url('') }}/pages/other/copyright-and-disclaimer">Copyright and Disclaimer</a> |
		<!--<a href="{{ url('') }}/site-map" target="_blank">Site Map</a> |-->
		<a href="{{ url('') }}/contact">Contact</a> |     
		<a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 

	</div>
</footer>