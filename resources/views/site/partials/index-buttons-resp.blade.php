 <div class="home-buttons-resp">
	  <div class="row m-0">         		   	 
		  <div class="col-12 home-buttons-box">
	         <a href="{{ url('') }}/pages/about-qorf">
		        <div class="home-buttons-box1">
			      <h2>About Us</h2>
				  <div class="home-buttons-box-img">	
                     <img src="{{ url('') }}/images/site/icon1a-aboutus.png" alt="About Us" />	
                     <img src="{{ url('') }}/images/site/icon1b-aboutus.png" alt="About Us" />	                  					   			   
				  </div>
			    </div>   			   
			 </a>
		  </div><!-- /.col-12 -->				
		
		  <div class="col-12 home-buttons-box">
		     <a href="{{ url('') }}/news/supported-research">
		        <div class="home-buttons-box2">
			       <h2>Supported Research</h2>	
				   <div class="home-buttons-box-img">	
                      <img src="{{ url('') }}/images/site/icon2a-research.png" alt="Research" />	
                      <img src="{{ url('') }}/images/site/icon2b-research.png" alt="Research" />	                   						   
				   </div>	   
			    </div> 
			  </a>			   
		  </div><!-- /.col-12 -->				  
		
		  <div class="col-12 home-buttons-box">
		     <a href="{{ url('') }}/donations">
		        <div class="home-buttons-box3">
			       <h2>Support Us</h2>	
				   <div class="home-buttons-box-img">	
                      <img src="{{ url('') }}/images/site/icon3a-supportus.png" alt="Support Us" />	
                      <img src="{{ url('') }}/images/site/icon3b-supportus.png" alt="Support Us" />	                   				
				   </div>	   
			    </div> 
			 </a> 			   
		  </div><!-- /.col-12 -->		
		  
		  <div class="col-12 home-buttons-box">
		     <a href="{{ url('') }}/news">
		        <div class="home-buttons-box4">
			       <h2>Events & News</h2>	
				   <div class="home-buttons-box-img">	
                      <img src="{{ url('') }}/images/site/icon4a-eventsnews.png" alt="Events & News" />
                      <img src="{{ url('') }}/images/site/icon4b-eventsnews.png" alt="Events & News" />                   				   
				   </div>	   
			    </div> 
			 </a>			   
		  </div><!-- /.col-12 -->		
		  
	   </div>

</div>
