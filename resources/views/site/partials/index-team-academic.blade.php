@if (isset($home_team_acaedmic))
	<div class="home-team home-team-academic"> 
	    <div class="panelNav">
			<h2>Our Academic Committee</h2>
			
			<div class="container-fluid">
			  <div id="carouselTeam" class="carousel slide" data-ride="carousel" data-interval="9000">
				<div id="carouselTeam-inner" class="carousel-inner row w-100 mx-auto" role="listbox">
				  @php
					 $counter = 0;
				  @endphp

				  @foreach ($home_team_acaedmic as $item)
					   <div class="carouselTeam-item carousel-item col-md-4 {{ ($counter == 0 ? 'active' : '') }} carousel-team-item">
					       <!-- <a href="{{ url('').'/'.$item->url }}"> -->
							   <div class="home-team-img">
								  <img src="{{ url('') }}/{{$item->photo}}" class="img-fluid mx-auto d-block" alt="{{$item->name}}">
							   </div>

							   <div class="home-team-name">{{$item->name}}</div>
							   <div class="home-team-title">{{$item->job_title}}</div>
						   <!-- </a> -->
					   </div>

					   @php
						$counter++;
					   @endphp
				  @endforeach     

				</div>
				<a class="carousel-control-prev" href="#carouselTeam" role="button" data-slide="prev">
				  <i class="fa fa-chevron-left fa-lg"></i>
				  <span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next text-faded" href="#carouselTeam" role="button" data-slide="next">
				  <i class="fa fa-chevron-right fa-lg"></i>
				  <span class="sr-only">Next</span>
				</a>
			  </div>
				
			  <!-- For Mobile Only -->
			  <div id="carouselTeam-resp">				  
				  <div class="row">
						
						@foreach ($home_team_acaedmic as $item)							
							<div class="col-12">
					          <!-- <a href="{{ url('').'/'.$item->url }}"> -->
								  <div class="home-team-img">
									 <img class="slide" src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}">
								  </div>

								  <div class="home-team-name">{{$item->name}}</div>
								  <div class="home-team-title">{{$item->job_title}}</div>
							  <!-- </a> -->
								
							</div>  
						@endforeach

				  </div>
			  </div>
			  <!-- END - For Mobile Only -->
				
			</div>						
		</div>
	</div>	 
@endif

@section('team-scripts')
	<script type="text/javascript">
		$("#carouselTeam").on("slide.bs.carousel", function(e) {
		  var $e = $(e.relatedTarget);
		  var idx = $e.index();
		  var itemsPerSlide = 3;
		  var totalItems = $(".carouselTeam-item").length;

		  if (idx >= totalItems - (itemsPerSlide - 1)) {
			var it = itemsPerSlide - (totalItems - idx);
			for (var i = 0; i < it; i++) {
			  // append slides to end
			  if (e.direction == "left") {
				$(".carouselTeam-item")
				  .eq(i)
				  .appendTo("#carouselTeam-inner");
			  } else {
				$(".carouselTeam-item")
				  .eq(0)
				  .appendTo("#carouselTeam-inner");
			  }
			}
		  }
		});
   </script>
@endsection