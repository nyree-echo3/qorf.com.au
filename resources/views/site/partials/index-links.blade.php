<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

<script>
$.fn.cycle.defaults.autoSelector = '.links-carousel';
</script>

<div class="home-links-panel">		
    <div class="container">
	   <div class="row">
		  <div class="col-lg-12">	
	       <h2>Thank you to our partners!</h2>
			<p>The important task of supporting orthopaedic research that improves musculoskeletal outcomes in Queensland has been made possible by the generous contributions we have received from our partners.   Together we can improve lives by supporting innovative orthopaedic research.  To find out more about our generous partners, please visit the links below.</p>
	   
	   	    <div class="btn-home-links">
				<a href='{{ url('') }}/pages/partner-with-us'>Partner with us</a>   
			 </div>	    		   
		   </div>
		</div>
	</div>		

    <div class="home-links">    
		<div class="links-carousel" 
		   data-cycle-fx=carousel
		   data-cycle-carousel-visible=5
			data-cycle-timeout=1
			data-cycle-speed=5000
			data-cycle-throttle-speed=true
			data-cycle-easing=linear
			data-cycle-slides=span
			>

				@foreach ($home_links as $home_link)
				   <span><a href='{{ $home_link->url }}' target='_blank' title='{!! strip_tags($home_link->description) !!}'><img src='{{ url('') }}/{{ $home_link->image }}' title='{!! strip_tags($home_link->description) !!}' alt='{{ $home_link->title }}' ></a></span>		
				@endforeach

		</div>
	</div> 
</div>
	