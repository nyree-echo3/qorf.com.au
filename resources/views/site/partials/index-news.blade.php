<img src="{{ url('') }}/images/site/icon-latestnews.png" alt="Latest News" />	
<h2>Latest News</h2>

@php 
   $counter = 0 
@endphp
        
@if (isset($home_news))	 
    <div id="newsCarousel" class="carousel slide carousel-fade" data-ride="carousel">  
		@foreach($home_news as $item)      
		    @php
              $counter++
	        @endphp
	         	 
			<div class="home-news-item carousel-item {{ $counter == 1 ? ' active' : '' }}">						   		  
			   <h3>{{ date("F Y", strtotime($item->start_date)) }}</h3>						       
			   <h4>{{ $item->title }}</h4>	
								   			   	           				       			   					 					   		   
			</div>					   
		@endforeach 
    </div>
    
     <div class="home-news-btn">
         <a class="btn-home-news" href="{{ url('') }}/news" role="button">more news</a>		
     </div>
@endif	
