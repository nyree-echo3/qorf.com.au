<div class="panel-supportus">
  <div class="home-supportus">
	<div class="container">
	   <div class="row">
		  <div class="col-lg-12">		     				     
			  {!! $home_supportus_text !!}
		  </div>
          
          <div class="col-lg-12">  
              <div class="panel-supportus-charity"><img src="{{ url('') }}/images/site/ACNC-Registered-Charity-Logo_RGB.png" title="ACNC Registered Charity Logo" alt="ACNC Registered Charity Logo"></div>	      
		  </div>
             
          <div class="col-lg-12">             
			 <div class="btn-home-supportus">
				<a href='{{ url('') }}/donations'>Learn More</a>   
			 </div>
		  </div>		
		   		  			  
	   </div>
	</div>
 </div>
</div>