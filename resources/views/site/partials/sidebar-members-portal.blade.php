@php
$href = request()->path();
@endphp

<div class="col-sm-3 blog-sidebar">
	<div class="sidebar-module">
		<h4>Members Portal</h4>	
		<ol class="list-group list-unstyled list-group-flush">
		    <!-- <li class="list-group-item {{ ($href == "members-portal" ? "active" : "") }}"><a href="{{ url('') }}/members-portal">Welcome</a></li> -->		    			
	       
	        {!! str_replace("nav-link", "", str_replace("nav-item", "list-group-item", $navigation)) !!}	        		     
	    </ol> 
	    
	    <!--
	    <br>
	    <h4>My Details</h4>	       
	    <ol class="list-group list-unstyled list-group-flush">	       
		  <li class="list-group-item {{ ($href == "members-portal/change-details" ? "active" : "") }}"><a class="navsidebar" href="{{ url('') }}/members-portal/change-details">Change My Details</a></li>
		  <li class="list-group-item  {{ ($href == "members-portal/change-password" ? "active" : "") }}"><a href="{{ url('') }}/members-portal/change-password">Change My Password</a></li>
		  <li class="list-group-item  {{ ($href == "members-portal/renew" ? "active" : "") }}"><a href="{{ url('') }}/members-portal/renew">Renew Membership</a></li>
		  <li class="list-group-item  {{ ($href == "members-portal/payments" ? "active" : "") }}"><a href="{{ url('') }}/members-portal/payments">Payments</a></li>
	     </ol>	
	     -->	
				
		<a class="btn-more" href="{{ url('') }}/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>								
	</div>
</div>