<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>Research</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @php               
                if ($categories) { 
                    foreach ($categories as $item) { 

                      $href = request()->path();

                      $style = '';
                      if($href==$item->url){
                         $style = 'active ';
                      }
            @endphp
            <li class='{{ $style }}list-group-item'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
            @php
                }                
            }
            @endphp
        </ol>
    </div>
</div>