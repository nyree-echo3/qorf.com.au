<div class="panel-links">			
	<h2>Thank you to our partners!</h2>
	<p>The important task of supporting orthopaedic research that improves musculoskeletal outcomes in Queensland has been made possible by the generous contributions we have received from our partners.   Together we can improve lives by supporting innovative orthopaedic research.  To find out more about our generous partners, please visit the links below.</p>	   	   	      		   		
    
	@foreach ($home_links as $home_link)
       <a href='{{ $home_link->url }}' target='_blank' title='{!! strip_tags($home_link->description) !!}'>
		   <div class="panel-links-item">		   
		      <div class="panel-links-img">
			     <img src='{{ url('') }}/{{ $home_link->image }}' title='{!! strip_tags($home_link->description) !!}' alt='{{ $home_link->title }}' >
			  </div>
			  
			  <div class="panel-links-txt">        
			     {!! $home_link->description !!}
			  </div>		   
		   </div>
	   </a>		
	@endforeach
	
</div>
	