@if (isset($panel_donations) && sizeof($panel_donations) > 0)
	<div class="panel-donors">		
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">	
					<h2>Thanks to our donors <i class="fas fa-heart"></i></h2>				   	   	      		   

					<marquee behavior="scroll" direction="left" scrollamount="{{ sizeof($panel_donations) }}" scrolldelay="{{ sizeof($panel_donations) * 10 }}">
						<p><span>
							@foreach ($panel_donations as $panel_donation)
								@php
									$donationText = "";

									$fields = json_decode($panel_donation->data);
									foreach ($fields as $field) {	

										if ($field->field == "name") {
											$donationText = $field->value;
										}
										if ($field->field == "name-last") {
											$donationText .= " " . $field->value;
										}
									}
								@endphp

							   {!! ($panel_donation->amount > 1000 ? "<b>" : "") !!} {{ $panel_donation->display_name }} {!! ($panel_donation->amount > 1000 ? "<b>" : "") !!}
							   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							@endforeach
						</span></p>
					</marquee>

				</div>
			</div>
		</div>		
	</div> 
@endif

	