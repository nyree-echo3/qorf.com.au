<?php
// Set Meta Tags
$meta_title_inner = "Renew Membership | " . $company_name;
$meta_keywords_inner = "Renew Membership " . $company_name;
$meta_description_inner = "Renew Membership " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-members-portal')

                <div class="col-sm-8 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Renew Membership</h1>
                        
                        @include('flash::message')

                        <table class="table">
                            <tbody>
                            <tr>
                                <th scope="row">Type</th>
                                <td>{{ $member->type->name.' - $'.number_format($member->type->price,2) }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Join Date</th>
                                <td>{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Expire Date</th>
                                <td>{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
                            </tr>
                            </tbody>
                        </table>                        

                        @if($member->isExpired() && !$has_flash_message)
                            @if($member->hasPendingRenewalPayment())
                                <div class="alert alert-primary" role="alert">
                                    You have a pending renewal payment! Your account will be renewed once the payment is approved.
                                </div>
                            @else
                                <div class="alert alert-danger" role="alert">
                                    Your account has been expired. Please renew your membership!
                                </div>
                            @endif
                        @endif

                        <form id="frmRenew" method="POST" action="{{ url('/members-portal/renew-process') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if($member->type->price > 0)
                            <div class="form-group row">
                                <label class="col-md-12 col-form-label"><h2>Payment</h2></label>
                                <div class="col-md-12 payment-options">
                                    <input type="radio" name="payment_method" value="paypal" checked>
                                    <img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal"><br>

                                    <input type="radio" name="payment_method" value="bank-deposit">
                                    <strong>Bank Deposit</strong><br />

                                    <label class="form-control-label"> {!!  $payment_direct_deposit_members !!} </label>
                                </div>
                            </div>
                            @endif

                            <div class="form-group row">
                                <div class="col-md-12 ">
                                    <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <div class="text-danger">{{ $errors->first('g-recaptcha-response') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn-checkout" name="signup" value="Sign up">Submit
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection

@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('inline-scripts')



@endsection