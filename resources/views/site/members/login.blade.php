<?php
// Set Meta Tags
$meta_title_inner = "Login | " . $company_name;
$meta_keywords_inner = "Login " . $company_name;
$meta_description_inner = "Login " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    <style>
    input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
    </style>
   
    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Login</h1>                       

                        <p>All members of the Australian Orthopaedic Association in Queensland are eligible for complimentary membership with QORF.</p>
                        <p>Please contact <a href="mailto:David.Parker@aoa.org.au?subject=QORF members login password">David.Parker@aoa.org.au</a> to obtain the members login password. </p>
                        
                        <form id="frmLogin" method="POST" action="{{ url('') }}/login">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="hidRedirect" value="{{ (isset($_GET['redirect']) ? $_GET['redirect'] : "") }}">

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">First Name *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="firstName" placeholder="Your first name"
                                           value="{{ old('firstName') }}"/>
                                    @if ($errors->has('firstName'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('firstName') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Last Name *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="lastName" placeholder="Your last name"
                                           value="{{ old('lastName') }}"/>
                                    @if ($errors->has('lastName'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('lastName') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Email *</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="email" placeholder="Your email"
                                           value="{{ old('email') }}"/>
                                    @if ($errors->has('email'))
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Password *</label>
                                <div class="col-md-9">
                                    <input type="password" class="form-control" name="password" placeholder="Enter password"/>

                                    @if($errors->any())                    
                                        <div class="fv-plugins-message-container">
                                            <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first() }}</div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <button type="submit" class="btn-checkout" name="login" value="Log In">Log In
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection


@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function (e) {

            const form = document.getElementById('frmLogin');
            FormValidation.formValidation(form, {
                    fields: {
                        firstName: {
                            validators: {
                                notEmpty: {
                                    message: 'The first name is required'
                                }
                            }
                        },
						lastName: {
                            validators: {
                                notEmpty: {
                                    message: 'The last name is required'
                               }
                            }
                        },
						email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required'
                                },
                                emailAddress: {
                                    message: 'The email is not valid'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
        });
    </script>

@endsection