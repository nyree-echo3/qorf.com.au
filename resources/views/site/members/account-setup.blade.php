<?php
// Set Meta Tags
$meta_title_inner = "Membership Account Set Up | " . $company_name;
$meta_keywords_inner = "Membership Account Set Up " . $company_name;
$meta_description_inner = "Membership Account Set Up " . $company_name;
?>

@extends('site.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('components/theme/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Membership Account Set Up</h1>
                        						
                        <p>Your application was considered by the Board and we delighted to advise that you have been accepted. </p>
                        @if($member->type->price>0)
                        <p>Your membership account now requires completion and payment. Once your payment is received you will have access to the Member Only Portal.</p>
                        @else
                        <p>Your membership account now requires completion. You will have access to the Member Only Portal once you setup your password.</p>
                        @endif
                                                                                                               
						<form id="frmSetup" enctype="multipart/form-data" method="POST" action="{{ url('') }}/register/store">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">      
							<input type="hidden" name="member_id" value="{{ $member_id }}">           						
							
							<h3>Account Details</h3>

							<div class="form-group row">
								<label class="col-md-3 col-form-label">Password *</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="password" placeholder="Your password" required />

									<div id="progressBarDiv">
										Strength
										<div class="progress mt-2" id="progressBar">
											<div class="progress-bar bg-secondary progress-bar-animate"
												 style="width: 100%"></div>
										</div>
									</div>

									@if ($errors->has('password'))
										<div class="fv-plugins-message-container">
											<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
										</div>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-3 col-form-label">Confirm Password *</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="passwordConfirm" placeholder="Confirm your password" required />
								</div>
							</div>

                            @if($member->type->price>0)
							<h3>Account Payment</h3>

							<div class="form-group row">
								<label class="col-md-3 col-form-label">Payment Type *</label>
								<div class="col-md-9 payment-options">
									<input type="radio" name="payment_method" value="paypal" checked>
									<img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal"><br />

									<input type="radio" name="payment_method" value="bank-deposit">
									<strong>Bank Deposit</strong><br />

									<label class="form-control-label"> {!!  $payment_direct_deposit_members !!} </label>
								</div>
							</div>
                            @endif

							<div class="form-group row has-error">
								<label class="col-md-3 col-form-label"></label>
								<div class="col-md-4 g-recaptcha-container">
									<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
									@if ($errors->has('g-recaptcha-response'))
										<div class="fv-plugins-message-container">
											<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
										</div>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-9 offset-md-3">
									<button type="submit" class="btn-checkout" name="signup" value="Sign up">Submit</button>
								</div>
							</div>
							
						</form>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

@endsection
@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>  
    <script src="{{ asset('components/theme/plugins/iCheck/icheck.min.js') }}"></script>       
@endsection
@section('inline-scripts')
    <script type="text/javascript">
		$( document ).ready(function() {
			$('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
			
		    $( "#btnAddQualification" ).click(function(e) {		
			  qualificationNum = parseInt($( "#hidQualifications" ).val()) + 1;
			  $( "#hidQualifications" ).val(qualificationNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Qualification #' + qualificationNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-qualification" name="qualification' + qualificationNum + '" placeholder="Qualification" value=""/>'; 
			  html += '<input type="text" class="form-control members-institution" name="institution' + qualificationNum + '" placeholder="Institution" value=""/>'; 
			  html += '<input type="text" class="form-control members-graduationYear" name="graduationYear' + qualificationNum + '" placeholder="Graduation year" value=""/>';       						
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divQualifications" ).append(html);	
				
			  e.preventDefault();
										
			});
			
			$( "#btnAddAwards" ).click(function(e) {		
			  awardsNum = parseInt($( "#hidAwards" ).val()) + 1;
			  $( "#hidAwards" ).val(awardsNum);
				
			  html  = '';
			  html += '<div class="form-group row">';  
			  html += '<label class="col-md-4 col-form-label">Honours & Awards #' + awardsNum + '</label>'; 
			  html += '<div class="col-md-8">'; 
			  html += '<input type="text" class="form-control members-awards" name="awards' + awardsNum + '" placeholder="Honours and Awards" value=""/>'; 			  					
			  html += '</div>'; 
			  html += '</div>';   
				
			  $( "#divAwards" ).append(html);	
				
			  e.preventDefault();
										
			});
			
		});
		
		
		
        document.addEventListener('DOMContentLoaded', function (e) {
            const strongPassword = function () {
                return {
                    validate: function (input) {
                        // input.value is the field value
                        // input.options are the validator options

                        const value = input.value;
                        if (value === '') {
                            return {
                                valid: true,
                            };
                        }

                        const result = zxcvbn(value);
                        const score = result.score;
                        const message = result.feedback.warning || 'The password is weak';

                        // By default, the password is treat as invalid if the score is smaller than 3
                        // We allow user to change this number via options.minimalScore
                        const minimalScore = input.options && input.options.minimalScore
                            ? input.options.minimalScore
                            : 3;

                        if (score < minimalScore) {
                            return {
                                valid: false,
                                // Yeah, this will be set as error message
                                message: message,
                                meta: {
                                    // This meta data will be used later
                                    score: score,
                                },
                            }
                        }
                    },
                };
            };

            const form = document.getElementById('frmRegister');
            FormValidation.formValidation(form, {
                    fields: {                                               
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                        passwordConfirm: {
                            validators: {
                                notEmpty: {
                                    message: 'The password confirmation is required'
                                },
                                identical: {
                                    compare: function () {
                                        return form.querySelector('[name="password"]').value;
                                    },
                                    message: 'The password and its confirm are not the same'
                                }
                            }
                        },                        
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
                .registerValidator('checkPassword', strongPassword)

                .on('core.validator.validating', function (e) {
                    if (e.field === 'password' && e.validator === 'checkPassword') {
                        document.getElementById('progressBarDiv').style.opacity = '1';
                    }
                })
                .on('core.validator.validated', function (e) {
                    if (e.field === 'password' && e.validator === 'checkPassword') {
                        const progressBar = document.getElementById('progressBar');

                        if (e.result.meta) {
                            // Get the score which is a number between 0 and 4
                            const score = e.result.meta.score;

                            // Update the width of progress bar
                            const width = (score == 0) ? '1%' : score * 25 + '%';
                            progressBar.style.opacity = 1;
                            progressBar.style.width = width;
                        } else {
                            progressBar.style.opacity = 0;
                            progressBar.style.width = '0%';
                        }
                    }
                });
        });				
		
    </script>
@endsection
