<?php 
   // Set Meta Tags
   $meta_title_inner = $page->meta_title; 
   $meta_keywords_inner = $page->meta_keywords; 
   $meta_description_inner = $page->meta_description;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($page->category->members_only == "active")
           @include('site/partials/sidebar-members-portal')        
        @else
           @include('site/partials/sidebar-pages')        
        @endif
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">
            @if (isset($page))
				<h1 class="blog-post-title">{{$page->title}}</h1>
				{!! $page->body !!}
           
                @if ($page->contact == "true")                   
                    @include('site/partials/contact-form')      
                @endif
                
                @if ($page->slug == "partner-with-us")       
                   @include('site/partials/panel-links')    
                @endif
            @endif
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
   
@endsection
