<?php 
   // Set Meta Tags
   $meta_title_inner = $news_item->meta_title; 
   $meta_keywords_inner = $news_item->meta_keywords; 
   $meta_description_inner = $news_item->meta_description;  
   $current_category = $news_item->category->id;
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @if ($news_item->category->members_only == "active")
           @include('site/partials/sidebar-members-portal')        
        @else
           @include('site/partials/sidebar-news')        
        @endif            
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">{{ $news_item->title }}</h1>
            
            @if ($category_slug == "newsletters")
                <div class='newsletter-container'>
            @endif
            
            <div class='news-item'>
               <div class='news-item-img'>
          	      @if($news_item->thumbnail != "")
			      <img src='{{ url('') }}/{{ $news_item->thumbnail }}' alt='{{ $news_item->title }}'>
			      @endif
			   </div>
              
               <div class='news-item-txt'>
			      {!! $news_item->body !!}
			   </div>          	             	            	            	   
         	  
          	 </div>
         	 
         	 @if ($category_slug == "newsletters")
                </div>
             @endif
                              	
           	 <a class='btn-more' href='{{ url('') }}/{{ $news_item->category->url }}'>< back</a>				 
          	   
           	 @include('site/partials/helper-sharing')	             					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
