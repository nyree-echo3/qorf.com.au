<?php 
   // Set Meta Tags
   $meta_title_inner = "Archived News"; 
   $meta_keywords_inner = "Archived News"; 
   $meta_description_inner = "Archived News";  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-news-archive')
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">Archived News</h1>
            
			@if(isset($items))  
                 @foreach($items as $item)                
                    <div class="col-lg-4">
                       <a href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
						   <div class="panel-news-item">	
								<!--<div class="div-img">
								<img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
								</div>-->				                                    

								<div class="panel-news-item-title">{{$item->title}}</div>
								<hr>
								<div class="panel-news-item-date">{{date("M Y", strtotime($item->start_date))}}</div>
								<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

								<div class="news-list-more"><a class="btn-home-news" href='{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}'>More <i class='fa fa-chevron-right'></i></a></div>		
						  </div>                                                 
					  </a>
                    </div>                    
                               
				 @endforeach                                         
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif  
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection
