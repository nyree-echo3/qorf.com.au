<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 

   $current_category = $category_id;
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">    
        @if ($category_members_only == "active")
           @include('site/partials/sidebar-members-portal')        
        @else
           @include('site/partials/sidebar-news')        
        @endif                                   
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">     
            <h1 class="blog-post-title">{{ $category_name }}</h1>
			
			@if ($category_description != "") 
               {!! $category_description !!}
            @endif         					          					
          
           <div class="blog-post row m-0">           
                               	        
            @if(isset($items))  
                 @foreach($items as $item)                                              

					<div class="col-sm-12 p-0">							     			   
						 <div class="panel-news-item">
					         <div class="panel-news-item-wrap">
						     <div class='news-item-img'>
								@if ($item->thumbnail != "")
									<img src='{{ url('') }}/{{ $item->thumbnail }}' alt='{{ $item->title }}'>
								@endif
							 </div>		
					     
					         <div class='news-item-txt {{ ($item->thumbnail != "" ? "news-item-txt-float" : "") }}'>
								 <h2>{{$item->title}}</h2>
								 <div class="panel-news-item-date">{{date("M Y", strtotime($item->start_date))}}</div>																								
								 {!! $item->short_description !!}

								 <a class="btn-more" href='{{ url('') }}/{{ $item->url }}'>Read More ></i></a>
						     </div>
							 </div>
						
						 </div>							 						 		  				  
					</div>        
                               
				 @endforeach                                         
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
             </div><!-- /.blog-post -->
             
             <!-- Pagination -->
             <div id="pagination">{{ $items->links() }}</div>                          
             
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->                
@endsection
