@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

<div class="container marketing divHome">

	<!-- Intro Text -->
	<div class="row featurette">
	  <div class="home-intro">
		<div class="container-fluid">
		   <div class="row m-0">
			  <div class="col-lg-6">
				 <?php echo $home_intro_text; ?>

				 <div class="btn-home-intro">
					<a href='{{ url('') }}/pages/about-qorf'>Learn More</a>   
				 </div>

			  </div>

			  <div class="col-lg-6">
				 <img src="{{ url('') }}/images/site/pic-circle2.jpg" title="Who we are & what we do" alt="Who we are & what we do"> 
			  </div>
		   </div>
	   </div>
	</div>

	</div>
	
</div>

@include('site/partials/index-team-board')
@include('site/partials/index-team-academic')
@include('site/partials/index-news-panel')
@include('site/partials/index-supportus')
@include('site/partials/index-parallax')
@include('site/partials/index-links')
@include('site/partials/panel-donations')

@endsection
