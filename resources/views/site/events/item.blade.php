<?php 
   // Set Meta Tags
   $meta_title_inner = $events_item->meta_title; 
   $meta_keywords_inner = $events_item->meta_keywords; 
   $meta_description_inner = $events_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-events')                   
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post">                          
            <h1 class="blog-post-title">{{ $events_item->title }}</h1>
                       
            <div class='events-item'>
               <div class='events-item-img'>
          	      @if($events_item->thumbnail != "")
			      <img src='{{ url('') }}/{{ $events_item->thumbnail }}' alt='{{ $events_item->title }}'>
			      @endif
			   </div>
             
               <div class="events-item-date">
				@if ($events_item->start_date != $events_item->end_date)
					{{date("D j M Y", strtotime($events_item->start_date))}} {{date("g:ia", strtotime($events_item->start_time))}} - {{date("D j M Y", strtotime($events_item->end_date))}} {{date("g:ia", strtotime($events_item->end_time))}}
				@else
					{{date("D j M Y", strtotime($events_item->start_date))}} - {{date("g:ia", strtotime($events_item->start_time))}} to {{date("g:ia", strtotime($events_item->end_time))}}
				@endif
			   </div>
              
			   <div class='events-item-book'>	
			      <input type="button" id="btnBooking" name="btnBooking" class="btn-checkout" value="Book" onclick="btnBooking_onclick('{{ $events_item->slug }}', '{{ url('') }}')" />
			   </div>
				
               <div class='events-item-txt'>
			      {!! $events_item->body !!}
			   </div>          	             	            	            	   
          	 </div>
			
			 <input type="button" id="btnBooking" name="btnBooking" class="btn-checkout" value="Book" onclick="btnBooking_onclick('{{ $events_item->slug }}', '{{ url('') }}')" />
			
			 <input type="hidden" id="hidMembersOnlyEvent" name="hidMembersOnlyEvent" value="{{ $events_item->members_only }}">
          	 <input type="hidden" id="hidIsMemberLoggedIn" name="hidIsMemberLoggedIn" value="{{ $isMemberLoggedIn }}">
			  
          	 <div class='btn-back'>
           	    <a class='btn-home-events' href='{{ url('') }}/events/{{ $events_item->category->slug }}'><i class="fas fa-chevron-left"></i> back</a>	
			 </div>
          	   
           	 @include('site/partials/helper-sharing')	             					  								  			            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

<div id="jQueryPopupDiv" style="display: none"></div>
<script src="{{ asset('js/site/events.js') }}"></script>
@endsection
