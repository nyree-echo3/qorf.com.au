<?php
// Set Meta Tags
$meta_title_inner = "Event Register | " . $company_name;
$meta_keywords_inner = "Event Register " . $company_name;
$meta_description_inner = "Event Register " . $company_name;
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    <style>
    input[type="text"]::-ms-input-placeholder, input[type="email"]::-ms-input-placeholder { color: #000!important; }
    </style>
   
    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 blog-main blog-wide">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Event Register</h1>

                        @include('flash::message')
                        
                        <div class='events-item'>
							<h2>{{ $event_item->title }}</h2>

							<div class="events-item-date">
								@if ($event_item->start_date != $event_item->end_date)
									{{date("D j M Y", strtotime($event_item->start_date))}} {{date("g:ia", strtotime($event_item->start_time))}} - {{date("D j M Y", strtotime($event_item->end_date))}} {{date("g:ia", strtotime($event_item->end_time))}}
								@else
									{{date("D j M Y", strtotime($event_item->start_date))}} - {{date("g:ia", strtotime($event_item->start_time))}} to {{date("g:ia", strtotime($event_item->end_time))}}
								@endif
							</div>                   
						</div>    

                        <form id="frmRegister" method="POST" action="{{ url('') }}/event-register/store">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">                            

                            <div class="form-wrapper">
                                <h2>Your {{ (isset($member) ? "Membership" : "") }} Details</h2>
                                
                                @php
                                   if (isset($member)) {
                                      $member_id = $member->id;
                                      $member_title = $member->title;
                                      $member_firstName = $member->firstName;
                                      $member_lastName = $member->lastName;
                                      $member_email = $member->email;
                                   } else  {
                                      $member_id = "";
                                      $member_title = "";
                                      $member_firstName = "";
                                      $member_lastName = "";
                                      $member_email = "";                                   
                                   }
                                @endphp
                                
                                <input type="hidden" name="member_id" value="{{ $member_id }}">
                                <input type="hidden" name="event_id" value="{{ $event_item->id }}">
                                <input type="hidden" name="event_slug" value="{{ $event_item->slug }}">
                                
								<div class="form-group row">
									<label class="col-md-3 col-form-label">Title *</label>
									<div class="col-md-9">
									    @if ($member_title != "")
									        <input type="text" class="form-control" name="title" placeholder="Your title" value="{{ old('title', $member_title) }}" readonly />
									    @else
											<select class="form-control" name="title" id="title" required>
												<option value="">Your title</option>
												<option value="Mr" {{ (old('title', $member_title) == "Mr") ? ' selected="selected"' : '' }}>Mr</option>
												<option value="Ms" {{ (old('title', $member_title) == "Ms") ? ' selected="selected"' : '' }}>Ms</option>
												<option value="Mrs" {{ (old('title', $member_title) == "Mrs") ? ' selected="selected"' : '' }}>Mrs</option>
												<option value="Dr" {{ (old('title', $member_title) == "Dr") ? ' selected="selected"' : '' }}>Dr</option>
												<option value="Assc. Professor" {{ (old('title', $member_title) == "Assc. Professor") ? ' selected="selected"' : '' }}>Assc. Professor</option>
												<option value="Professor" {{ (old('title', $member_title) == "Professor") ? ' selected="selected"' : '' }}>Professor</option>
											</select>
										@endif
										
										@if ($errors->has('title'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('title') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">First Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="firstName" placeholder="Your first name" value="{{ old('firstName', $member_firstName) }}" {{ ($member_firstName != "" ? "readonly" : "") }} />
										@if ($errors->has('firstName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('firstName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Last Name *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="lastName" placeholder="Your last name" value="{{ old('lastName', $member_lastName) }}" {{ ($member_lastName != "" ? "readonly" : "") }}/>
										@if ($errors->has('lastName'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('lastName') }}</div>
											</div>
										@endif
									</div>
								</div>

								<div class="form-group row">
									<label class="col-md-3 col-form-label">Email Address *</label>
									<div class="col-md-9">
										<input type="text" class="form-control" name="email" placeholder="Your email address"  value="{{ old('email', $member_email) }}" {{ ($member_email != "" ? "readonly" : "") }}/>
										@if ($errors->has('email'))
											<div class="fv-plugins-message-container">
												<div class="fv-plugins-bootstrap fv-help-block"> {{ $errors->first('email') }}</div>
											</div>
										@endif
									</div>
								</div>
							</div>
                            
                            <div class="form-wrapper">
                                <h2>Your Ticket</h2>
                                
								<div class="form-group row">
									<div class="col-md-12">
									    <select class="form-control" id="event_ticket" name="event_ticket" placeholder="Your ticket">
									        @php
								               $counterPayment = 0;
									           $paymentHide = true;
									        @endphp
									        
									    	@foreach ($event_tickets as $event_ticket)		
								    	       @php
								    	       if ($counterPayment == 0 && $event_ticket->price > 0)  {
								    	          $paymentHide = false;
								    	       } 
								    	       $counterPayment++;
								    	       @endphp
								    	       						    	       
									    	   <option value="{{ $event_ticket->id }}">{{ $event_ticket->name }} ${{ number_format($event_ticket->price, 2) }}</option>									    	   
									    	@endforeach	
									    </select>										
									</div>																		
								</div> 																
							</div>
                          
                            @foreach ($event_tickets as $event_ticket)									   
							   <input type="hidden" id="hidTicketValue{{ $event_ticket->id }}" value="{{ $event_ticket->price }}">								    	   
							@endforeach	 
                           
                            <div class="form-wrapper form-wrapper-white {{ ($paymentHide ? "paymentHide" : "") }}" id="divPayment">                            								
								<h2>Payment Type *</h2>								

								<div class="form-group row">									
									<div class="col-md-12 payment-options">
										<input type="radio" name="payment_method" value="paypal" checked>
										<img src="{{ url('') }}/images/site/Paypal.png" title="PayPal" alt="PayPal"><br />

										<input type="radio" name="payment_method" value="bank-deposit">
										<strong>Bank Deposit</strong><br />

										<label class="form-control-label"> {!!  $payment_direct_deposit_events !!} </label>
									</div>
								 </div>
							 </div>
                            
                             <div class="form-group row has-error">
								<label class="col-md-12 g-recaptcha-container">
									<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
									@if ($errors->has('g-recaptcha-response'))
										<div class="fv-plugins-message-container">
											<div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
										</div>
									@endif
								</div>
							</div>                                                                                                            

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn-checkout" name="register" value="Register">Register</button>
                                </div>
                            </div>                            
                        </form>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection


@section('scripts')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('inline-scripts')

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function (e) {

            const form = document.getElementById('frmLogin');
            FormValidation.formValidation(form, {
                    fields: {
                        email: {
                            validators: {
                                notEmpty: {
                                    message: 'The email is required'
                                },
                                emailAddress: {
                                    message: 'The email is not valid'
                                }
                            }
                        },
                        password: {
                            validators: {
                                notEmpty: {
                                    message: 'The password is required'
                                },
                                checkPassword: {
                                    message: 'The password is too weak',
                                    minimalScore: 2,
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        icon: new FormValidation.plugins.Icon({
                            valid: 'fa fa-check',
                            invalid: 'fa fa-times',
                            validating: 'fa fa-refresh',
                        }),
                    },
                }
            )
        });
				
		$( "#event_ticket" ).change(function() {
		    ticket_id = $("#event_ticket").val();
			ticket_val = parseFloat($("#hidTicketValue" + ticket_id).val());
		  
			if (ticket_val > 0)  {
				 $("#divPayment").removeClass("paymentHide");				
			} else  {
				 $("#divPayment").addClass("paymentHide");
			}
			
		});
    </script>

@endsection