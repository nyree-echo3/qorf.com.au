<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="">
    <meta name="description" content="">
	<meta name="author" content="Echo3 Media">
	<meta name="web_author" content="www.echo3.com.au">
	<meta name="date" content="" scheme="DD-MM-YYYY">
	<meta name="robots" content="all" >
    <title></title>

    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700%7CRoboto+Slab:300,400,700" rel="stylesheet"> 
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link href="{{ asset('/css/site/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css') }}">
    
    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?" >
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png" >
   
    
            
  </head>
  <body>        
	  <p>Holding Page</p>
	  
<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>

@yield('scripts')
@yield('inline-scripts')
</body>
</html>
