@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-testimonials')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post testimonials">          
            @if (isset($items))        
				@foreach ($items as $item)	
			        <div class='testimonials-item'>          
			           <div class='testimonials-item-txt'>                       
				          {!! $item->description !!}	
					   </div>		
					   <div class='testimonials-item-person'>  	    				
					      {{$item->person}}					
					   </div>					
			        </div>
				@endforeach 
            @endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
