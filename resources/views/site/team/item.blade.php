@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">
    <div class="container">

        <div class="row">
            @include('site/partials/sidebar-pages')

            <div class="col-sm-9 blog-main">
                <section class="team-block cards-team">
                    <div class="container">

                        <div class="blog-post">                            
                            <div class='row'>
                                <div class="col-8">
                                    <div class="col-12"><h1 class="blog-post-title">{{ $team_member->name }}</h1></div>
                                    
                                    @if($team_member->job_title)
                                    <div class="col-12"><h2>{{ $team_member->job_title }}</h2></div>
                                    @endif
                                    @if($team_member->role)
									<div class="col-12"><h3>{{ $team_member->role }}</h3></div>
                                    @endif
                                    @if($team_member->phone)
                                    <div class="col-12"><strong>Phone</strong> : {{ $team_member->phone }}</div>
                                    @endif
                                    @if($team_member->mobile)
                                    <div class="col-12"><strong>Mobile</strong> : {{ $team_member->mobile }}</div>
                                    @endif
                                    @if($team_member->email)
                                    <div class="col-12"><strong>Email</strong> : {{ $team_member->email }}</div>
                                    @endif
                                    @if($team_member->body)
                                    <div class="col-12">{!! $team_member->body !!}</div>
                                    @endif
                                </div>
                                @if ($team_member->photo)
                                    <div class='col-4'>                                        
                                        <img src="{{ url('') }}{{$team_member->photo}}" alt="{{$team_member->name}}" class="card-img-top">                                                                                
                                    </div>
                                @endif

                            <div class="col-8">
								<div class="col-12">
									<div class='btn-pagination'>
								        @php
								            $foundPrev = false;
								            $foundNext = false;
								            $prevSlug = "";
								        @endphp
								        
									    @foreach ($allmembers as $currentmember)  							              
								            @if (!$foundPrev && $prevSlug != "" && $currentmember->id == $team_member->id) 
								                <a class='btn-prev' href='{{ url('') }}/team/{{ $currentmember->category->slug }}/{{ $prevSlug }}'>< Previous</a>
								                
								                @php
								                   $foundPrev = true;								            
								                @endphp
								            @endif
								            
									        @if ($foundNext)
											   <a class='btn-next' href='{{ url('') }}/team/{{ $currentmember->category->slug }}/{{ $currentmember->slug }}'>Next ></a>
											   @php
											      break;
											   @endphp
											@endif
											
											@php
											  if ($currentmember->id == $team_member-> id)  {
											   $foundNext = true;
											  }
											@endphp	
											
											@php
											   $prevSlug = $currentmember->slug;								    																			    											
											@endphp
										@endforeach
									</div>
								</div>
							</div>
                           
                            </div>

                            
                        </div>
                    </div>
                </section>
              
            </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->

@endsection