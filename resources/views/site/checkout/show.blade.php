@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        <!--include('site/partials/sidebar-products')-->
                     
        <div class="col-sm-8 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">Checkout</h1>
                @include('flash::message')     
                
                @unless ($checkout)
					<div class="alert alert-info">
						<p>Hey, nothing to check out here!</p>
					</div>
				@endunless

				@if ($checkout)
					<form id="checkout" action="{{ route('checkout.submit') }}" method="post">
						{{ csrf_field() }}

						@include('site.checkout._billpayer', ['billpayer' => $checkout->getBillPayer()])

						<div>
							<input type="hidden" name="ship_to_billing_address" value="0" />
							<div class="checkbox">
								<label>
									<input type="checkbox" name="ship_to_billing_address" value="1"
										   v-model="shipToBillingAddress">
									Ship to the same address
								</label>
							</div>
						</div>

						<hr>

						@include('site.checkout._shipping_address', ['address' => $checkout->getShippingAddress()])

						<h3>Total: {{ format_price($checkout->total()) }}</h3>

						<hr>

						<div>
							<button class="btn-checkout">Submit Order</button>
						</div>


					</form>
				@endif
            
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection
                                                                      
@section('scripts')
    <script src="{{ asset('js/site/app.js') }}"></script>
   
    @if ($checkout)
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            new Vue({
                el: '#checkout',
                data: {
                    isOrganization: {{ old('billpayer.is_organization') ?: 0 }},
                    shipToBillingAddress: {{ old('ship_to_billing_address') ?? 1 }}
                }
            });
        });
    </script>
    @endif
@stop

