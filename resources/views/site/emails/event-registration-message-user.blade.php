<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">

<p>Dear {{ $event_booking->first_name }},</p>
<p>Thank you for your event registration.</p>
<p>Please find attached your receipt.</p>

<h2>Event Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Event :</th>
        <td align="left">{{ $event->title }}</td>
    </tr>
    <tr>
        <th align="left">Date/Time :</th>
        <td align="left">
        	@if ($event->start_date != $event->end_date)
				{{date("D j M Y", strtotime($event->start_date))}} {{date("g:ia", strtotime($event->start_time))}} - {{date("D j M Y", strtotime($event->end_date))}} {{date("g:ia", strtotime($event->end_time))}}
			@else
				{{date("D j M Y", strtotime($event->start_date))}} - {{date("g:ia", strtotime($event->start_time))}} to {{date("g:ia", strtotime($event->end_time))}}
			@endif
        </td>
    </tr>
    <tr>
        <th align="left" style="width:150px">Your Ticket :</th>
        <td align="left">{{ $ticket->name }} (${{ number_format($ticket->price, 2) }})</td>
    </tr>    
    <tr>     
        <th align="left">Details</th>  
        <td align="left">{!! $event->short_description !!}</td>
    </tr>
    <tr>     
        <th align="left"></th>  
        <td align="left">View more details <a href='{{ url('') }}/events/{{ $event->category->slug }}/{{ $event->slug }}'>here</a>.</td>
    </tr>   
</table>

@if($event_payment)	
	<h2>Payment Details</h2>
	<table class="table">
		<tr>
			<th style="width:100px" align="left">Method :</th>
			<td align="left">
				@if($event_payment->payment_method=='paypal')
					PayPal
				@endif
				@if($event_payment->payment_method=='bank-deposit')
					Bank Deposit
				@endif
			</td>
		</tr>

		<tr>
			<th align="left">Amount :</th>
			<td align="left">${{ number_format($event_payment->amount,2) }}</td>
		</tr>

		<tr>
			<th align="left">Status :</th>
			<td align="left">{{ ucfirst($event_payment->payment_status) }}</td>
		</tr>
		
		@if($event_payment->payment_method=='bank-deposit')
		    <tr>
			   <th align="left"></th>
			   <td align="left">
			      {!! $payment_direct_deposit_events !!}
			      <p><b>Your registration will be activated once your payment is approved.</b></p>
			   </td>
		    </tr>
		@endif
		

		@if($event_payment->payment_method=='paypal')
		<tr>
			<th align="left">Transaction Number :</th>
			<td align="left">{{ $event_payment->payment_transaction_number }}</td>
		</tr>
		<tr>
			<th align="left">PayPal Payer ID :</th>
			<td align="left">{{ $event_payment->paypal_payer_id }}</td>
		</tr>
		@endif
		<tr>
			<th align="left">&nbsp</th>
			<td align="left">&nbsp</td>
		</tr>
	</table>
@endif

<p>Kind regards,<br>
Queensland Orthopaedic Research Fund (QORF)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
