<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<br><br>
<h1>Website | Membership Payment</h1>
<p>The payment can be reviewed <a href='{{ url('') }}/dreamcms/members/{{ $payment->id }}/edit-payment'>here</a>.</p>

<h2>Member Details</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Type :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>
    <tr>
        <th align="left">Full Name :</th>
        <td align="left">{{ $member->full_name }}</td>
    </tr>
    <tr>
        <th align="left">Phone Number :</th>
        <td align="left">{{ $member->phoneMobile }}</td>
    </tr>
    <tr>
        <th align="left">Email :</th>
        <td align="left">{{ $member->email }}</td>
    </tr>
    <tr>
        <th align="left">Address :</th>
        <td align="left">{{ $member->address1.' '.$member->address2 }}</td>
    </tr>
    <tr>
        <th align="left">Suburb :</th>
        <td align="left">{{ $member->suburb }}</td>
    </tr>
    <tr>
        <th align="left">State :</th>
        <td align="left">{{ $member->state }}</td>
    </tr>
    <tr>
        <th align="left">Postcode :</th>
        <td align="left">{{ $member->postcode }}</td>
    </tr>
    <tr>
        <th align="left">Join Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateJoin)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Expire Date :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dateExpire)->format('d-m-Y') }}</td>
    </tr>
</table>

@if($payment->payment_method=='bank-deposit')
    <p>In order to active the member, please change the status of the renewal payment to "Completed" once the payment has been received.</p>
@endif

<h2>Payment Details</h2>
<table class="table">
    <tr>
        <th style="width:100px" align="left">Method :</th>
        <td align="left">
            @if($payment->payment_method=='paypal')
                PayPal
            @endif
            @if($payment->payment_method=='bank-deposit')
                Bank Deposit
            @endif
        </td>
    </tr>

    <tr>
        <th align="left">Amount :</th>
        <td align="left">${{ number_format($payment->amount,2) }}</td>
    </tr>

    <tr>
        <th align="left">Status :</th>
        <td align="left">{{ ucfirst($payment->payment_status) }}</td>
    </tr>

    @if($payment->payment_method=='paypal')
    <tr>
        <th align="left">Transaction Number :</th>
        <td align="left">{{ $payment->payment_transaction_number }}</td>
    </tr>
    <tr>
        <th align="left">PayPal Payer ID :</th>
        <td align="left">{{ $payment->paypal_payer_id }}</td>
    </tr>
    @endif
    <tr>
        <th align="left">&nbsp</th>
        <td align="left">&nbsp</td>
    </tr>
</table>

<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
