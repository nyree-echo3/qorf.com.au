<!DOCTYPE html>
<html>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Dear {{ $member->firstName }},</p>

<p>Thank you for your application for the Ordinary membership. Your application was considered by the Board and I am delighted to advise that you have been awarded Ordinary member status of the Australian and New Zealand Sarcoma Association (ANZSA). </p>

<p>
As an Ordinary member, you are entitled to the following benefits:
<ul>
<li>Attend ANZSA’s Annual Scientific Meeting (discounts for members) and other related workshops</li>	
<li>Access to CPD accredited events</li>	
<li>Eligible to apply for ANZSA’s Sarcoma Research Grants Program</li>	
<li>Access to ANZSA’s national sarcoma database</li>	
<li>Access to ANZSA’s research support</li>	
<li>Opportunities to collaborate and network</li>	
<li>Contribute to sarcoma research and clinical trials</li>	
<li>Eligible to vote and to hold office bearer roles</li>	
<li>Receive frequent ANZSA’s news updates and announcements</li>	
</ul>
</p>

<p>
As an Ordinary member:
<ul>
<li>You are expected to contribute to the sarcoma community by devoting a significant component of your professional activity towards the research, science or treatment of sarcoma.</li>	
<li>You are required to attend our Annual Scientific Meeting a minimum of once (1) in three years.</li>	
<li>You are encouraged to play an active role in the organisation. We value your voice and are open to queries, ideas and suggestions you may have. Feel free to write to us at <a href="mailto:contact@sarcoma.org.au">contact@sarcoma.org.au</a>.</li>	
</ul>	
</p>

<p>Ordinary members are required to pay an annual membership fee of AUD150. Your membership account now requires completion and payment by visiting the link below. </p>

<p><a href='{{ url('') }}/register/{{ $member->id }}'>{{ url('') }}/register/{{ $member->id }}</a></p>

<p>I look forward to welcoming you personally and to working closely with you as we aim to continue improving outcomes for sarcoma and related tumours, patients and their families. </p>

<p>Sincerely,<br>
Adrian Cosenza<br>
Chair
</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
