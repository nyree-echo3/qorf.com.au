<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #891637 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #891637 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #891637 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi,</p>
<p>Thank you for submitting your grant application to QORF.</p>
<p>Kind regards,<br>
Queensland Orthopaedic Research Fund (QORF)</p>
<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
