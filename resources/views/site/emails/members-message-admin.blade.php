<!DOCTYPE html>
<html>
<head>
<style>
	h1 { color: #2f67b2 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
	h2 { color: #2f67b2 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #2f67b2 solid;}
</style>	
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<h1>Website | Membership Application</h1>
<p>The application can be reviewed and approved <a href='{{ url('') }}/dreamcms/members/{{ $member->id }}/edit'>here</a>.</p>

<h2>1. Membership Class</h2>
<table class="table">
    <tr>
        <th align="left" style="width:150px">Membership Class Applied For :</th>
        <td align="left">{{ $member->type->name }}</td>
    </tr>    
</table>

<h2>2. Personal Information</h2>
<table class="table">    
    <tr>
        <th align="left">Title :</th>
        <td align="left">{{ $member->title }}</td>
    </tr>
    <tr>
        <th align="left">First Name :</th>
        <td align="left">{{ $member->firstName }}</td>
    </tr>
    <tr>
        <th align="left">Last Name :</th>
        <td align="left">{{ $member->lastName }}</td>
    </tr>
    <tr>
        <th align="left">Former Names (if applicable) :</th>
        <td align="left">{{ $member->formerName }}</td>
    </tr>
    <tr>
        <th align="left">Date Of Birth :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->dob)->format('d-m-Y') }}</td>
    </tr>
</table>

<h2>3. Personal Contact Details</h2>
<table class="table">    
    <tr>
        <th align="left">Address :</th>
        <td align="left">{{ $member->address1.' '.$member->address2 }}</td>
    </tr>
    <tr>
        <th align="left">Suburb :</th>
        <td align="left">{{ $member->suburb }}</td>
    </tr>
    <tr>
        <th align="left">State :</th>
        <td align="left">{{ $member->state }}</td>
    </tr>
    <tr>
        <th align="left">Postcode :</th>
        <td align="left">{{ $member->postcode }}</td>
    </tr>
    <tr>
        <th align="left">Country :</th>
        <td align="left">{{ $member->country }}</td>
    </tr>
    <tr>
        <th align="left">Phone Number :</th>
        <td align="left">{{ $member->phoneLandline }}</td>
    </tr>
    <tr>
        <th align="left">Mobile :</th>
        <td align="left">{{ $member->phoneMobile }}</td>
    </tr>
    <tr>
        <th align="left">Email :</th>
        <td align="left">{{ $member->email }}</td>
    </tr>
</table>

<h2>4. Qualification Details</h2>
<table class="table">  
    @php
       $counter = 0;
    @endphp
    
    @if (sizeof($qualifications) > 0)
        @foreach ($qualifications as $qualification)
           @php
              $counter++;
           @endphp
           
           <tr>
              <th align="left">Qualification #{{ $counter }} :</th>
              <td align="left">{{$qualification->qualification}}<br>{{$qualification->institution}}<br>{{$qualification->graduationYear}}</td>
           </tr>                  
        @endforeach  
     @endif 
     
     @php
       $counter = 0;
    @endphp
    
    @if (sizeof($awards) > 0)
        @foreach ($awards as $award)
           @php
              $counter++;
           @endphp
           
           <tr>
              <th align="left">Honours & Awards #{{ $counter }} :</th>
              <td align="left">{{$award->award}}</td>
           </tr>                  
        @endforeach  
     @endif                                       
</table>

<h2>5. Employment Details</h2>
<table class="table">           
    <tr>
        <th align="left">Discipline :</th>
        <td align="left">{{ $member->occupation }}</td>
    </tr>
    <tr>
        <th align="left">Employer :</th>
        <td align="left">{{ $member->companyName }}</td>
    </tr>
    <tr>
        <th align="left">Date of Commencement :</th>
        <td align="left">{{ \Carbon\Carbon::parse($member->employmentDateCommencement)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <th align="left">Address :</th>
        <td align="left">{{ $member->employmentAddress1.' '.$member->employmentAddress2 }}</td>
    </tr>
    <tr>
        <th align="left">Suburb :</th>
        <td align="left">{{ $member->employmentSuburb }}</td>
    </tr>
    <tr>
        <th align="left">State :</th>
        <td align="left">{{ $member->employmentState }}</td>
    </tr>
    <tr>
        <th align="left">Postcode :</th>
        <td align="left">{{ $member->employmentPostcode }}</td>
    </tr>
    <tr>
        <th align="left">Country :</th>
        <td align="left">{{ $member->employmentCountry }}</td>
    </tr>
    <tr>
        <th align="left">Phone Number :</th>
        <td align="left">{{ $member->employmentPhoneNumber }}</td>
    </tr>    
    <tr>
        <th align="left">Email :</th>
        <td align="left">{{ $member->employmentEmail }}</td>
    </tr>
</table>

<h2>6. Other Relevant Experience</h2>
<table class="table">           
    <tr>
        <th align="left">Other Relevant Experience :</th>
        <td align="left">{{ $member->otherExperience }}</td>
    </tr>
    
    <tr>
        <th align="left">CV File :</th>
        <td align="left">
           @if ($member->cvFile != "")
              File attached to email
           @else
             Not provided
           @endif
        </td>
    </tr>
</table>

<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>
