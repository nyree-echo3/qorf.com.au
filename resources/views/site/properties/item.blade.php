<?php
// Set Meta Tags
$meta_title_inner = ($property->category->name . " - Properties");
$meta_keywords_inner = "Properties";
$meta_description_inner = ($property->category->name . " - Properties");
?>

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/fotorama/fotorama.css') }}">
@endsection

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">

                <div class="col-sm-12 blog-main">

                    <div class="row">
                        <div class="col-3">

                            <div class="col-12 property-detail-box">
                                {{ $property->unit.' / '.$property->street_number.' '.$property->street_name }} </br>
                                {{ $property->municipality }}
                                <hr />
                                @if($property->category->slug=="rent")
                                    FOR RENT<br />
                                    ${{ number_format($property->rental_per_week,2) }} PW
                                @endif
                                @if($property->category->slug=="sale")
                                    FOR SALE<br />
                                    ${{ number_format($property->price,2) }}
                                @endif
                                <hr />
                                @if($property->statement_of_information_pdf)
                                <a href="{{ $property->statement_of_information_pdf }}" target="_blank"><i class="far fa-file-pdf"></i> Statement of Information</a>
                                @endif

                                @if($property->floorplans)
                                <br/><button id="floorplan" type="button" class="btn btn-link" data-href="{{ $property->floorplans->first()->location }}"><i class="far fa-file-image"></i> Floorplan</button>
                                @endif

                                <br/><a href="/media/due-diligence-checklist.pdf" target="_blank"><i class="far fa-file"></i>  Due Diligence Checklist</a>

                                <hr />

                                @if($property->leadagent)
                                <div class="agent">
                                    <img src="{{ $property->leadagent->photo }}" class="rounded-circle" alt="{{ $property->leadagent->name }}" width="54" align="left">
                                    <strong>{{ $property->leadagent->name }}</strong><br>
                                    {{ $property->leadagent->mobile }}<br>
                                    {{ $property->leadagent->phone }}<br>
                                    <a href="mailto:{{ $property->leadagent->email }}">{{ $property->leadagent->email }}</a>
                                </div>
                                <hr />
                                @endif

                                @if($property->dualagent)
                                <div class="agent">
                                    <img src="{{ $property->dualagent->photo }}" class="rounded-circle" alt="{{ $property->dualagent->name }}" width="54" align="left">
                                    <strong>{{ $property->dualagent->name }}</strong><br>
                                    {{ $property->dualagent->mobile }}<br>
                                    {{ $property->dualagent->phone }}<br>
                                    <a href="mailto:{{ $property->dualagent->email }}">{{ $property->dualagent->email }}</a>
                                </div>
                                @endif
                            </div>

                        </div>
                        <div class="col-9">

                            <div class="property-header">

                                <div class="row">
                                    <div class="col-7 header-suburb">
                                        {{ $property->suburb }}
                                    </div>
                                    <div class="col-5 header-icons">
                                        {{ $property->bedrooms }} <i class="fas fa-bed mr-1"></i>
                                        {{ $property->bathrooms }} <i class="fas fa-shower mr-1"></i>
                                        {{ $property->garage_spaces }} <i class="fas fa-car"></i>
                                    </div>
                                </div>

                            </div>
                            <div class="fotorama" data-nav="thumbs" data-thumbheight="60" data-width="100%">
                                @foreach($property->images as $image)
                                <img src="{{ $image->location }}">
                                @endforeach
                            </div>

                            <div>{!! $property->description !!}</div>

                            <div id="map">map</div>

                        </div>

                    </div>

                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCQFWVXABWZm0M-JiYnNs-6EWFuOpM5woA"></script>
    <script src="{{ asset('/components/fotorama/fotorama.js') }}"></script>
    <script src="{{ asset('/js/site/property.js') }}"></script>
@endsection

@section('inline-scripts')
    <script>

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
        });

        var geocoder= new google.maps.Geocoder();

        geocoder.geocode( { 'address': '1 / 49 Springvale Road Melbourne VIC'}, function(results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                //alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    </script>
@endsection

