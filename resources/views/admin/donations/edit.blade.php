@extends('admin/layouts/app')

@section('styles')    
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/donations') }}"><i class="fas fa-donate"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/donations/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $donation->id }}">
                            <div class="box-body">                        

                                <div class="form-group {{ ($errors->has('display_name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Display Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="display_name" name="display_name" class="form-control"
                                                   value="{{ old('display_name',$donation->display_name) }}">                                            
                                        </div>

                                        @if ($errors->has('display_name'))
                                            <small class="help-block">{{ $errors->first('display_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                            @php
                                if(count($errors)>0){
                                   if(old('display')=='true'){
                                    $display = 'active';
                                   }else{
                                    $display = '';
                                   }
                                }else{
                                    $display = $donation->display;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Display my donation details on the QORF website * </label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="display" {{ $display == 'true' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/donations') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                                                               
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

   
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {          

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection 