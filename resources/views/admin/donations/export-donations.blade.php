 <table>
    <thead>
    <tr>
        <th><b>Donations Export</b></th>
	</tr>      
      
    <tr>
    	<th><b><u>Report Creation:</u></b></th>
    	<th>{{ date('d M Y ga') }}</th>
    </tr>
     
    <tr>
        <th><b>Total Donations:</b></th> 
        <th>{{ sizeof($donations) }}</th>   	 
    </tr> 
      
    <tr>
        <th></th>
	</tr>              
       
    <tr>        
        @foreach(json_decode($donations[0]->data) as $field)									
		   <th><b>{{ ucwords(strip_tags($field->label)) }}</b></th>				
		@endforeach
                        
        <th><b>Payment Method</b></th>
        <th><b>Payment Date</b></th>
        <th><b>Payment Status</b></th>
        <th><b>PayPal Transaction Number</b></th>
        <th><b>PayPal Transaction Result</b></th>               
    </tr>
    </thead>
    <tbody>
    @foreach($donations as $item)
        <tr>
            @foreach(json_decode($item->data) as $field)									
			   <td>
			      @if (trim(strip_tags($field->label)) == "Donation type") 
					 @if ($field->value == "once") 
					    Give Once
					 @else
					    Give Regularly
					 @endif               
				  @else
					 {{ $field->value }}
				  @endif
			   </td>				
		    @endforeach
                                                                         
            <td>{{ ucwords(str_replace("-", " ", $item->payment_method)) }}</td>
            <td>{{ date('d/m/Y' , strtotime($item->created_at)) }}</td>
            <td>{{ ucwords($item->payment_status) }}</td>
            <td>{{ $item->payment_transaction_number }}</td>
            <td>{{ $item->payment_transaction_result }}</td>                                                                                  
        </tr>
    @endforeach

    </tbody>
</table>