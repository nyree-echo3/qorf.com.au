 <table>
    <thead>
    <tr>
        <th><b>Members Export</b></th>
	</tr>      
      
    <tr>
    	<th><b><u>Report Creation:</u></b></th>
    	<th>{{ date('d M Y ga') }}</th>
    </tr>
     
    <tr>
        <th><b>Total Members:</b></th> 
        <th>{{ sizeof($members) }}</th>   	 
    </tr> 
      
    <tr>
        <th></th>
	</tr>              
       
    <tr>              
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>        
        <th><b>Email</b></th>      
    </tr>
    </thead>
    <tbody>
    @foreach($members as $member)
        <tr>            
            <td>{{ $member->firstName }}</td>
            <td>{{ $member->lastName }}</td>            
            <td>{{ $member->email }}</td>           
        </tr>
    @endforeach

    </tbody>
</table>