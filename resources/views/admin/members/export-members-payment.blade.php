 <table>
    <thead>
    <tr>
        <th><b>Members Payment Export</b></th>
	</tr>      
      
    <tr>
    	<th><b><u>Report Creation:</u></b></th>
    	<th>{{ date('d M Y ga') }}</th>
    </tr>
     
    <tr>
        <th><b>Total Payments:</b></th> 
        <th>{{ sizeof($payments) }}</th>   	 
    </tr> 
      
    <tr>
        <th></th>
	</tr>              
       
    <tr>        
        <th><b>Member</b></th>
        <th><b>Payment Type</b></th>
        <th><b>Payment Method</b></th>
        <th><b>Payment Amount</b></th>
        <th><b>Payment Status</b></th>        
        <th><b>Payment Date/Time</b></th>        
    </tr>
    </thead>
    <tbody>
    @foreach($payments as $item)
        <tr>           
            <td>{{ $item->member->firstName }} {{ $item->member->lastName }} (ID: {{ $item->member->id }})</td>
            <td>{{ ucwords($item->payment_type) }}</td>
            <td>{{ ucwords(str_replace("-", " " , $item->payment_method)) }}</td>
            <td>${{ $item->amount }}</td>            
            <td>{{ ucwords($item->payment_status) }}</td>            
            <td>{{ date('d/m/Y' , strtotime($item->created_at)) }}</td>            
        </tr>
    @endforeach

    </tbody>
</table>