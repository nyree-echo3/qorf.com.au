<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DreamCMS | Login</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset('/components/theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('/components/theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/admin/general.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">       
    <div class="login-logo">       
        <div class="login-logo-img">
           <img src="{{ asset('/images/admin/logo.png') }}" alt="Echo3" title="Echo3" class='navbar-logo'>    
        </div>
       
        <a href="../../index2.html">Dream<b>CMS</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Please login with your valid credentials.</p>

        <form method="post" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ ($errors->has('email')) ? ' has-error' : '' }}">
                <input type="text" id="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback {{ ($errors->has('password')) ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" placeholder="Password" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">

                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                </div>
            </div>
        </form>

    </div>
</div>

<footer class="login-footer">
    <div>
    </div>
    Copyright &copy; 2011 - {{ date('Y') }} <span>|</span> DreamCMS V4.0 <span>|</span> <a href='https://www.echo3.com.au' target='_blank'>Echo3</a>
</footer>

<script src="{{ asset('/components/theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('/components/theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>
</body>
</html>
