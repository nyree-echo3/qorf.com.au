<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    Copyright &copy; 2011 - {{ date('Y') }} <span>|</span> DreamCMS V4.0 <span>|</span> <a href='https://www.echo3.com.au' target='_blank'>Echo3</a>
</footer>