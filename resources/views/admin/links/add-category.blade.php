@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/links') }}"><i class="fas fa-link"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/links/categories') }}">Categories</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Category</h3>
                    </div>

                    <form method="post" class="form-horizontal" action="{{ url('dreamcms/links/store-category') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="box-body">

                            <div class="form-group {{ ($errors->has('name')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Category Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name"
                                           placeholder="Category Name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <small class="help-block">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="box-footer">
                                <a href="{{ url('dreamcms/links/categories') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
            
        });        
    </script>
@endsection
