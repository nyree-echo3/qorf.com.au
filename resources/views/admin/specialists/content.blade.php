@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Find a Sarcoma Specialist</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-clipboard"></i> Find a Sarcoma Specialist</a></li>
                <li class="active">Page Content </li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">                       
                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/specialists/update-content') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                
                                <div class="form-group {{ ($errors->has('content')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Page Content</label>

                                    <div class="col-sm-10">
                                        <textarea type="text" class="form-control" name="content" rows='5' placeholder="Address">{{ $content->value }}</textarea>
                                        @if ($errors->has('content'))
                                            <small class="help-block">{{ $errors->first('content') }}</small>
                                        @endif
                                    </div>
                                </div>

                            </div>
                                                                                                   
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {  
			CKEDITOR.replace('content');			
        });
    </script>
@endsection