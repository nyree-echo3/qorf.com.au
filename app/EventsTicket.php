<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsTicket extends Model
{   
    protected $table = 'events_tickets';
    
    public function tickets()
    {
        return $this->hasMany(Events::class, 'event_id')->where('status', '=', 'active');
    }
	
	public function active_tickets()
    {
        return $this->hasMany(Events::class, 'event_id')->where('status', '=', 'active');
    }   
}
