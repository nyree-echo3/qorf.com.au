<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberQualification extends Model
{

    protected $table = 'members_qualifications';

}
