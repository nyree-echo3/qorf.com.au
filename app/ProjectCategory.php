<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class ProjectCategory extends Model
{
    use SortableTrait;

    protected $table = 'project_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'category_id');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','projects')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'projects/'.$this->attributes['slug'];
    }
}
