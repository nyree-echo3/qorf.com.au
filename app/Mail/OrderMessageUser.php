<?php

namespace App\Mail;

use App\ContactMessages;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public function __construct($order)
    {
        $this->order = $order;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName . " | Order")
			        ->from($contactEmail)
			        ->view('site/emails/order-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
					));
    }
}
