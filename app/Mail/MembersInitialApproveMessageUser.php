<?php

namespace App\Mail;

use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class MembersInitialApproveMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $member;

    public function __construct($member)
    {
        $this->member = $member;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;

        $setting = Setting::where('key','=','company-name')->first();
        $companyName = $setting->value;
		
        return $this->subject($companyName.' | Membership Initial Payment Approve')
			        ->from($contactEmail)
			        ->view('site/emails/members-initial-approve-message-user');
    }
}
