<?php

namespace App\Mail;

use App\GrantsMessages;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GrantsMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $grants_message;

    public function __construct(GrantsMessages $grants_message)
    {
        $this->grants_message = $grants_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName)
			        ->from($contactEmail)
			        ->view('site/emails/grants-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
					));
    }
}
