<?php

namespace App\Mail;

use App\GrantsMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class GrantsMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $grants_message;

    public function __construct(GrantsMessages $grants_message)
    {
        $this->grants_message = $grants_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Grant Application')
			        ->from($contactEmail)
			        ->view('site/emails/grants-message-admin');
    }
}
