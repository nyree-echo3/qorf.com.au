<?php

namespace App\Mail;

use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class MembersRenewApproveMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $member;
    public $payment;

    public function __construct($member, $payment)
    {
        $this->member = $member;
        $this->payment = $payment;
    }

    public function build()
    {
        $setting = Setting::where('key','=','company-name')->first();
        $companyName = $setting->value;

        $setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName.' | Membership Renewal Payment Approve')
			        ->from($contactEmail)
			        ->view('site/emails/members-renew-approve-message-user');
    }
}
