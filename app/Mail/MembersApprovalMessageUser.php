<?php

namespace App\Mail;

use App\Member;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembersApprovalMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $member;

    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    public function build()
    {		
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;				
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		switch ($this->member->type_id)  {
			// Ordinary	 
			case 1:	
					return $this->subject($companyName.' | Ordinary Member Acceptance')
								->from($contactEmail)					
								->view('site/emails/members-approval-message-ordinary', array(
									'companyName' => $companyName,		
								));
				    break;
			
			// Ordinary	- Consumer 
			case 2:	
					return $this->subject($companyName.' | Ordinary Member Acceptance')
								->from($contactEmail)					
								->view('site/emails/members-approval-message-ordinary-consumer', array(
									'companyName' => $companyName,		
								));
				    break;
				
			// Associate	 
			case 3:	
					return $this->subject($companyName.' | Associate Member Acceptance')
								->from($contactEmail)					
								->view('site/emails/members-approval-message-associate', array(
									'companyName' => $companyName,		
								));
				    break;
				
			// Corresponding	 
			case 4:	
					return $this->subject($companyName.' | Corresponding Member Acceptance')
								->from($contactEmail)					
								->view('site/emails/members-approval-message-corresponding', array(
									'companyName' => $companyName,		
								));
				    break;
				
			// Corresponding	 
			case 5:	
					return $this->subject($companyName.' | Corresponding Member Acceptance')
								->from($contactEmail)					
								->view('site/emails/members-approval-message-corresponding-consumer', array(
									'companyName' => $companyName,		
								));
				    break;
				
		}
		
    }
}
