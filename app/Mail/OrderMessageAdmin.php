<?php

namespace App\Mail;

//use App\ContactMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class OrderMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    public function __construct($order)
    {  
		$this->order = $order;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | Order')
			        ->from($contactEmail)
			        ->view('site/emails/order-message-admin');
    }
}
