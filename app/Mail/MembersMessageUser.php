<?php

namespace App\Mail;

use App\Member;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembersMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $member;

    public function __construct($member)
    {
        $this->member = $member;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;				
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;

        $payment_direct_deposit_members = Setting::where('key', '=', 'payment-direct-deposit-members')->first();
		
        return $this->subject($companyName.' | Membership Application')
			        ->from($contactEmail)
			        ->view('site/emails/members-message-user', array(
						'companyName' => $companyName, 												
					));
    }
}
