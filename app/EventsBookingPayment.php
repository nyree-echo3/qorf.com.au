<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class EventsBookingPayment extends Model
{
    use Sortable;

    public $sortable = ['amount', 'payment_type', 'payment_method', 'payment_status', 'created_at'];
    protected $table = 'events_bookings_payments';

	public function newQuery()
    {
        return parent::newQuery()->where('is_deleted', '=', 'false');
    }
	
    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

//    public function scopeFilter($query)
//    {
//
//        $filter = session()->get('member-payment-filter');
//        $select = "";
//
//        if($filter['payment_type'] && $filter['payment_type']!="all"){
//            $select =  $query->where('payment_type', $filter['payment_type']);
//        }
//
//        if($filter['payment_method'] && $filter['payment_method']!="all"){
//            $select =  $query->where('payment_method', $filter['payment_method']);
//        }
//
//        if($filter['payment_status'] && $filter['payment_status']!="all"){
//            $select =  $query->where('payment_status', $filter['payment_status']);
//        }
//
//        if($filter['daterange']){
//            $range_arr = explode(" - ",$filter['daterange']);
//            $start = Carbon::createFromFormat('d/m/Y',$range_arr[0])->format('Y-m-d');
//            $finish = Carbon::createFromFormat('d/m/Y',$range_arr[1])->format('Y-m-d');
//
//            $select =  $query->whereBetween('created_at',[$start.' 00:00:00',$finish.' 23:59:59']);
//        }
//
//        return $select;
//    }

}
