<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = 'donations';
	
	 protected $fillable = [
        'data',
        'amount',
        'display',
        'display_name',
        'payment_type',
		'payment_method',
		'payment_status',
		'payment_transaction_result'
    ];
	
    public function newQuery()
    {
        return parent::newQuery();
    }   
}
