<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CancelLinkValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $hash = md5($request->id.env('CANCEL_LINK_SECRET'));

        if($hash==$request->hash){
            return $next($request);
        }

        abort(404);
    }
}
