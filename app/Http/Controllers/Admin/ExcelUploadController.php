<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Member;
use App\MemberPayment;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ExcelUploadController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	public function __construct()
    {
        $this->middleware('auth');				
    }
	
    public function importExport()
    {
        return view('admin/upload/importExport');
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function downloadExcel($type)
    {

        $data = Member::get()->toArray();
         
        return Excel::create('Export Data', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function importExcel(Request $request)
    {		
        $request->validate([
            'import_file' => 'required'
        ]);


        $path = $request->file('import_file')->getRealPath();
		
        $data = Excel::load($path)->get();

        if($data->count()){
//			dd($data);
			$member_id = 0;
            foreach ($data as $key => $value) {	
				$title = $value->title;
				
				switch (strtolower($title)) {
					case "prof":  $title = "Professor"; break;
					case "a/prof":  $title = "Assc. Professor"; break;
				}
				
				switch (strtolower($value->membership_class)) {
					case "ordinary": 
						if (strtolower($value->consumer) == "no") {
						   $type_id = 1; 
						   $type_amt = 150;	
						} else  {
						   $type_id = 2;	
						   $type_amt = 10;	
						}	
						break;
					case "associate": 
						$type_id = 3;
						$type_amt = 100;
						break;		
					case "corresponding":
						if (strtolower($value->consumer) == "no") {
						   $type_id = 4; 
						   $type_amt = 50;	
						} else  {
						   $type_id = 5;	
						   $type_amt = 10;	
						}	
						break;										
				}

				// Member
				$member_id++;
				$member[] = [
					'id' => $member_id, 
					'type_id' => $type_id, 
					'title' => $value->title, 
					'firstName' => $value->first_name,
					'lastName' => $value->last_name,
					//'formerName' => '',
					'dob' => date('Y-m-d', strtotime($value->dob)),
					'occupation' => $value->profession,
					'companyName' => $value->employer_institution,
					'address1' => ' ',
					//'address2' => '',
					'suburb' => ' ',
					'state' => $value->australian_state,
					'postcode' => ' ',
					'country' => $value->country,
					//'phoneLandline' => '',
					'phoneMobile' => str_replace(" ", "", $value->mobile),
					'email' => $value->email,
					//'employmentDateCommencement' => '',
					'employmentAddress1' => ' ',
					'employmentAddress2' => ' ',
					'employmentSuburb' => ' ',
					'employmentState' => ' ',
					'employmentPostcode' => ' ',
					'employmentCountry' => ' ',
					'employmentPhoneNumber' => str_replace(" ", "", $value->work_phone),
					//'employmentEmail' => '',
					//'otherExperience' => '',
					'comments' => $value->comments,
					'dateJoin' => date('Y-m-d', strtotime($value->application_received)),					
					'dateExpire' => date('Y-m-d', strtotime($value->application_received . '+1 years')),
					'password' => Hash::make(date('Ymdhis')),
					'status' => (strtolower($value->review_outcome) == "approved" ? 'active' : 'passive'),
					'payment_method' => 'bank-deposit',
					'payment_status' => ($value->membership_payment != "" ? 'completed' : 'pending'),
					'created_at' => date('Y-m-d', strtotime($value->application_received)),			
					'updated_at' => date('Y-m-d'),			
				];
				
				// Member Payment
				$memberPayment[] = [
					'id' => $member_id, 
					'member_id' => $member_id, 
					'amount' => $type_amt, 
					'payment_type' => 'initial',
					'payment_method' => 'bank-deposit',					
					'payment_status' => ($value->membership_payment != "" ? 'completed' : 'pending'),			
					'created_at' => date('Y-m-d', strtotime($value->application_received)),			
					'updated_at' => date('Y-m-d'),			
				];
            }

            if(!empty($member)){
                Member::insert($member);
            }
			
			if(!empty($memberPayment)){
                MemberPayment::insert($memberPayment);
            }
        }

        return back()->with('success', 'Insert Record successfully.');
    }
}