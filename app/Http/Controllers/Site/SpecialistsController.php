<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

use App\Specialist;
use App\Module;
use App\Setting;

class SpecialistsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
		$module = Module::where('slug', '=', "specialists")->first();
		
		$content = Setting::where('key','=','specialists-content')->first();
		
        $side_navV2 = (new NavigationBuilder())->buildSideNavigation('pages/about-sarcoma');

        $side_nav_mode = 'manual';
		
	    $category_name = "Find A Specialist";	
		
	    $items = Specialist::where('status', '=', 'active')->orderBy('position', 'asc')->get();				
				
		$au_states = Specialist::select("state")->where('country', '=', 'Australia')->where('status', '=', 'active')->get()->toArray();	
		foreach($au_states as $key => $val) {
			$new_au_states[] = $val['state'];
		}
		$au_states = array_unique($new_au_states);			
		
		$nz_states = Specialist::select("state")->where('country', '=', 'New Zealand')->where('status', '=', 'active')->get()->toArray();	
		foreach($nz_states as $key => $val) {
			$new_nz_states[] = $val['state'];
		}
		$nz_states = array_unique($new_nz_states);			
		
		return view('site/specialists/list', array(
			'module' => $module,
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,			
			'items' => $items,
			'au_states' => $au_states,
			'nz_states' => $nz_states,
			'content' => $content->value,   
         ));

    }				  			
}
