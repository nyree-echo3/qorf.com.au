<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use App\Member;
use App\Events;
use App\EventsCategory;
use App\EventsTicket;
use App\EventsBooking;
use App\EventsBookingPayment;
use App\Setting;
use App\Module;

use App\Mail\EventRegistrationMessageAdmin;
use App\Mail\EventRegistrationMessageUser;

use Srmklive\PayPal\Services\ExpressCheckout;

class EventsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
        $module = Module::where('slug', '=', "events")->first();
		
		if ($category_slug == "")  {
		   // Get Latest Events
		   $category_name = "Events";	
		   $category_description = "";
		   $category_members_only = "passive";
			
		   $items = $this->getEvents(1);

		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category Events	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
		  $category_description = $category->description;	
		  $category_members_only = $category->members_only;	
			
		  $items = $this->getEvents($category->id);
		}

        if($category_members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($category_members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }

        $side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		return view('site/events/list', array(   
			'module' => $module,
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'category_name' => $category_name,
			'category_description' => $category_description,
			'category_members_only' => $category_members_only,
			'items' => $items,			
        ));

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
		
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  			
		$events_item = $this->getEventsItem($item_slug, $mode);			  
			
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($events_item->category->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }
		
		return view('site/events/item', array(     
			'module' => $module,
			'isMemberLoggedIn' => $isMemberLoggedIn,			
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'events_item' => $events_item,
			'mode' => $mode,
			'category_slug' => $category_slug
        ));
    }
	
	public function register($event_slug){
		// Logged in Member
		$member = Auth::guard('member')->user();
		if ($member == null)  {
		   $isMemberLoggedIn = false;
		} else  {
		   $isMemberLoggedIn = true;
		}
		
		$module = Module::where('slug', '=', "events")->first();
    	$side_nav = $this->getCategories();				  	
		
		$event_item = $this->getEventsItem($event_slug, "");			  
		$event_tickets = $this->getEventTickets($event_item->id, $isMemberLoggedIn);
		
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Direct Deposit text
        $payment_direct_deposit_events = Setting::where('key', '=', 'payment-direct-deposit-events')->first();
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
        $side_nav_mode = 'manual';
        if($side_nav==null){
            $side_nav = $this->getCategories();
            $side_nav_mode = 'auto';
        }				

        if($event_item->members_only=='active' && Auth::guard('member')->user()==null){
            return redirect('login');
        }

        if($event_item->members_only=='active' && Auth::guard('member')->user()->isExpired()){
            return redirect('members-portal/renew');
        }
		
		return view('site/events/register', array(     
			'module' => $module,
			'company_name' => $company_name, 
			'payment_direct_deposit_events' => $payment_direct_deposit_events->value,
			'member' => $member, 
			'side_nav' => $side_nav,
			'side_nav_mode' => $side_nav_mode,
			'event_item' => $event_item,			
			'event_tickets' => $event_tickets,			
        ));
    }
	
	public function registerStore(Request $request)
    {		
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

        // Google Recaptcha Validation
        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }

        $member = Member::where('id','=',$request->member_id)->first();
		$event = Events::where('id','=',$request->event_id)->first();
		$ticket = EventsTicket::where('id','=',$request->event_ticket)->first();

        // Field Validation
        $rules = array(
            'title' => 'required',
			'firstName' => 'required',
			'lastName' => 'required',
			'email' => 'required',
        );

        $messages = [
            'title.required' => 'Please select title',
            'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'email.required' => 'Please enter email',
        ];

        if ($ticket->price > 0) {
            $rules['payment_method'] = 'required';
            $messages['payment_method.required'] = 'Please select payment method';
        }

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('event-register/' . $request->event_slug)->withErrors($validator)->withInput();
        }				
		
		// Create Event Booking
        // ********************
		$event_booking = new EventsBooking();
		$event_booking->event_id = $request->event_id;
		$event_booking->ticket_id = $request->event_ticket;
		$event_booking->member_id = $request->member_id;
		$event_booking->title = $request->title;
		$event_booking->first_name = $request->firstName;
		$event_booking->last_name = $request->lastName;
		$event_booking->email_address = $request->email;
		$event_booking->save();
		
		
		if ($ticket->price == 0){
			$this->sendRegistrationEmail($event, $ticket, $event_booking);
			
            return $this->redirectInfoPage('success', 'Thank you for your event registration!  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.', $request->event_id);
        }

		// Process Payment
        // ***************
        if ($request->payment_method == 'bank-deposit') {

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;            
            $event_payment->payment_method = 'bank-deposit';
            $event_payment->payment_status = 'pending';
            $event_payment->save();

            $this->sendRegistrationEmail($event, $ticket, $event_booking, $event_payment);

            return $this->redirectInfoPage('success', 'Thank you for your event registration! Your registration will be finalised once your payment is received.  Your registration ID is ' . $event_booking->id . '.  A confirmation email has been sent to you.', $request->event_id);
        }

        if ($request->payment_method == 'paypal') {

            $provider = new ExpressCheckout;

            $data = $this->generatePaymentData($event, $ticket, $event_booking);
            $response = $provider->setExpressCheckout($data);

            if ($response['ACK'] == 'Failure') {            
                return $this->redirectInfoPage('error', 'There was an error on payment. Please try again!');
            }

            $event_payment = new EventsBookingPayment();
            $event_payment->booking_id = $event_booking->id;
            $event_payment->amount = $ticket->price;           
            $event_payment->payment_method = 'paypal';
            $event_payment->payment_status = 'pending';
            $event_payment->paypal_token = $response['TOKEN'];
            $event_payment->save();

            return redirect($response['paypal_link']);
        }
    }
	
    private function generatePaymentData($event, $ticket, $booking)
    {

        $data = [];
        $data['items'] = [[
            'name' => 'ANZSA Event - '.$event->title,
            'price' => $ticket->price,
            'qty' => 1
        ]];

        $data['invoice_id'] = 'booking-'.$booking->id;
        $data['invoice_description'] = "Event Booking #" . $booking->id . " Invoice";
        $data['return_url'] = url('/event-register/paypal-success');
        $data['cancel_url'] = url('/event-register/paypal-cancel');
        $data['total'] = $ticket->price;

        return $data;
    }
	
    public function paypalSuccess(Request $request)
    {

        $event_payment = EventsBookingPayment::where('paypal_token','=',$request->token)->first();
		
		$booking = EventsBooking::where('id','=',$event_payment->booking_id)->first();
		$event = Events::where('id','=',$booking->event_id)->first();
		$ticket = EventsTicket::where('id','=',$booking->ticket_id)->first();		
        $member = Member::where('id','=',$booking->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($event, $ticket, $booking);

        $payment_response = $provider->doExpressCheckoutPayment($data, $event_payment->paypal_token, $request->PayerID);

        if ($payment_response['ACK'] != 'Success') {
            $event_payment->delete();
            $booking->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!', $event->id);
        }

        $checkout_details = $provider->getExpressCheckoutDetails($event_payment->paypal_token);

        if ($checkout_details['CHECKOUTSTATUS'] != 'PaymentActionCompleted') {
            $event_payment->delete();
            $booking->delete();
            return $this->redirectInfoPage('error', 'Your payment was not successful. Please try again!', $event->id);
        }

        $event_payment->payment_status = "completed";
        $event_payment->payment_transaction_number = $checkout_details['TRANSACTIONID'];
        $event_payment->paypal_payer_id = $request->PayerID;
        $event_payment->is_processed = "true";
        $event_payment->save();

        $this->sendRegistrationEmail($event, $ticket, $booking, $event_payment);

        return $this->redirectInfoPage('success', 'Thank you for your event registration! Your payment was successful.  Your registration ID is ' . $booking->id . '.  A confirmation email has been sent to you.', $event->id);
    }	
	
	public function paypalCancel(Request $request)
    {

        $event_payment = EventsBookingPayment::where('paypal_token','=',$request->token)->first();
		
		$booking = EventsBooking::where('id','=',$event_payment->booking_id)->first();
		$event = Events::where('id','=',$booking->event_id)->first();
		$ticket = EventsTicket::where('id','=',$booking->ticket_id)->first();		
        $member = Member::where('id','=',$booking->member_id)->first();

        $provider = new ExpressCheckout;
        $data = $this->generatePaymentData($event, $ticket, $booking);

        $payment_response = $provider->doExpressCheckoutPayment($data, $event_payment->paypal_token, $request->PayerID);
        
		$event_payment->delete();
		$booking->delete();
		return $this->redirectInfoPage('error', 'You cancelled your payment. Please try again!', $event->id);
    }	
	
	private function redirectInfoPage($type, $message, $event_id)
    {
        $company_name = Setting::where('key', '=', 'company-name')->first();
        $company_name = $company_name->value;

		$event_item = Events::where('id','=',$event_id)->first();
		
        if ($type == 'success') {
            flash($message);
        }

        if ($type == 'error') {
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");
        }

        return view('site/events/info', array(
			'company_name' => $company_name,
			'event_item' => $event_item
		));
    }
	
	public function getCategories(){
		$categories = EventsCategory::whereHas("events")->where('status', '=', 'active')->get();
		foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getEvents($category_id = "", $limit = 12){
		$today = date("Y/m/d");		
		
		if ($category_id == "")  {
			$events = Events::has("categorypublic")
			            ->where('status', '=', 'active')
						->where('start_date', '>=', $today)											        
						->orderBy('start_date', 'desc')				        
						->paginate($limit);	
		} else {
		   $events = Events::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)
						->where('start_date', '>=', $today)						
						->orderBy('start_date', 'desc')
						->paginate($limit);		
		}
		
		return($events);
	}		
	  
	public function getEventsItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $events = Events::where(['slug' => $item_slug])->first();							
		} else {
		   $events = Events::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($events);
	}
	
	public function getEventTickets($event_id, $members_only){
		if ($members_only)  {
		   $tickets = EventsTicket::where(['event_id' => $event_id, 'is_deleted' => "false", 'members_only' => "active"])->get();							
		} else  {
		   $tickets = EventsTicket::where(['event_id' => $event_id, 'is_deleted' => "false", 'members_only' => "passive"])->get();								
		}
		
		return($tickets);
	}
	
	public function getCategory($category_slug){

		$categories = EventsCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
	
	private function sendRegistrationEmail($event, $ticket, $event_booking, $event_payment = NULL)
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new EventRegistrationMessageAdmin($event, $ticket, $event_booking, $event_payment));       

        // Email User
        Mail::to($event_booking->email_address)->send(new EventRegistrationMessageUser($event, $ticket, $event_booking, $event_payment));

    }
}
