<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Link;
use App\News;
use App\Page;
use App\Project;
use App\TeamMember;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class SiteSearchController extends Controller
{

    public function index(Request $request){

        $page_results = Page::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $news_results = News::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $project_results = Project::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $team_results = TeamMember::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $links_results = Link::search($request->get('query'))
            ->where('status','active')
            ->where('is_deleted','false')
            ->get();

        $highest = max([
            count($news_results),
            count($page_results),
            count($project_results),
            count($team_results),
            count($links_results)
        ]);

        $results = array();

        for($i=0; $i<$highest; $i++){

            if(!empty($page_results[$i])){
                $page = [
                    'type' => 'page',
                    'item' => $page_results[$i]
                ];
                $results[] = $page;
            }

            if(!empty($news_results[$i])){
                $news = [
                    'type' => 'news',
                    'item' => $news_results[$i]
                ];
                $results[] = $news;
            }

            if(!empty($project_results[$i])){
                $project = [
                    'type' => 'project',
                    'item' => $project_results[$i]
                ];
                $results[] = $project;
            }

            if(!empty($team_results[$i])){
                $team = [
                    'type' => 'team',
                    'item' => $team_results[$i]
                ];
                $results[] = $team;
            }

            if(!empty($links_results[$i])){
                $link = [
                    'type' => 'link',
                    'item' => $links_results[$i]
                ];
                $results[] = $link;
            }
        }

        $results = $this->paginate($results, [
            'path' => 'results?query='.$request->get('query')
        ]);

        return view('site/site-search/results', array(
            'query' => $request->get('query'),
            'results' => $results
        ));
    }

    public function paginate($items, $options = [], $perPage = 25, $page = null)
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
