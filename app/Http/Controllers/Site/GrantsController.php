<?php

namespace App\Http\Controllers\Site;

use App\GrantsMessages;
use App\Http\Controllers\Controller;
use App\Mail\GrantsMessageAdmin;
use App\Mail\GrantsMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

class GrantsController extends Controller
{
    public function index(){

        $fields = Setting::where('key', '=', 'grants-form-fields')->first();
      
        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/grants/grants', array(
            'form' => $form,            
        ));
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('grant-application')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'grants-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new GrantsMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new GrantsMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new GrantsMessageUser($message));

        return \Redirect::to('grant-application/success');
    }

    public function success(){				
        return view('site/grants/success', array(                      
        ));
    }		
}
