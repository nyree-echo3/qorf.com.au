<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';

    public function category()
    {
        return $this->belongsTo(GalleryCategory::class, 'category_id');
    }

    public function scopeFilter($query)
    {
        $filter = session()->get('gallery-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('name','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}