<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagesHomeSlider extends Model
{
    protected $table = 'images_home_slider';
}