<?php
namespace App\Imports;

use App\Donation;
use App\Setting;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class DonationsImport implements ToModel, WithHeadingRow, WithBatchInserts
{
    public function model(array $row)
    {	
		if ($row["invoice_details"] != "Totals")  {		
			$fields = Setting::where('key', '=', 'donation-form-fields')->first();
			$form = $fields->value;
			
			$form = rtrim($form, ']');
			$form = $form . ',{"type":"text","subtype":"text","required":true,"label":" Donation type","className":"form-control","name":"type","readonly":"readonly","placeholder":""}, {"type":"text","subtype":"text","required":true,"label":" Donation amount","className":"form-control","name":"donation","readonly":"readonly"}]';

			$fields = json_decode($form);

			$data = array();

			$display = "false";	
			$display_name = "";
			$display_name_last = "";

			foreach ($fields as $field) {
				if ($field->type != 'header' && $field->type != 'paragraph') {
					$field_value = "";

					if ($field->name == "name")  {
						$field_value = $row["first_name"];	
					}

					if ($field->name == "name-last")  {
						$field_value = $row["last_name"];	
					}

					if ($field->name == "type")  {
						$field_value = "once";	
					}
					if ($field->name == "display")  {
						$field_value = "false";	
					}

					if ($field->name == "donation")  {
						$field_value = "$". number_format($row["a3_queensland_orthopaedic_research_foundation_donation"], 2);	
					}

					$data_field['label'] = $field->label;
					$data_field['field'] = $field->name;
					$data_field['value'] = $field_value;
					array_push($data, $data_field);				
				}
			}	
			
			return new Donation([
				'message'     => 'test',
				'data'    => json_encode($data),
				'amount'    => $row['a3_queensland_orthopaedic_research_foundation_donation'],
				'display'    => 'true',
				'display_name'    => $row["first_name"] . " " . $row["last_name"],
				'payment_type'    => 'once',
				'payment_method'    => 'bank-deposit',
				'payment_status'    => 'completed',
				'payment_transaction_result'    => 'Successful'			
			]);
			
		}
    }
	
	public function batchSize(): int
    {
        return 1000;
    }
} 