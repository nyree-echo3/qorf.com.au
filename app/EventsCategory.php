<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class EventsCategory extends Model
{
    use SortableTrait;

    protected $table = 'events_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function events()
    {
        return $this->hasMany(Events::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function active_events()
    {
        return $this->hasMany(Events::class, 'category_id')->where('status', '=', 'active');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','events')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'events/'.$this->attributes['slug'];
    }
}
