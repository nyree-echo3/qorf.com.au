<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

use App\Member;
use App\Setting;
use App\Mail\MembersRenewReminderMessageUser;

class EmailMembersRenewReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:members-renew-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email Members Renewal Reminders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Contact Email
        $setting = Setting::where('key', '=', 'contact-email')->first();
        $contactEmail = $setting->value;       
		
		// Expiry Date in two weeks
		$dateExpiryTwoWeeks = date('Y-m-d', strtotime("+2 week"));
			
		// Active Members expirying in two weeks        		
		$members = Member::where('status', '=', 'active')->where('dateExpire', '=', $dateExpiryTwoWeeks)->get();   
		
        // Email Users
		foreach ($members as $member)  {
           Mail::to($member->email)->send(new MembersRenewReminderMessageUser($member));
		}

    }
}
