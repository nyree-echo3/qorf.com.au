<?php
namespace App\Exports;

use App\Donation;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class DonationsExport implements FromView, WithEvents
{
    public function view(): View
    {
		//$donations = Donation::get();
		$donations = Donation::where('payment_status','=','completed')
                ->orWhere('payment_status','=','regular_payment_cancelled')->get();
		
        return view('admin/donations/export-donations', [
            'donations' => $donations
        ]);
    }
	
	public function registerEvents(): array
    {		
        return [
			AfterSheet::class    => function(AfterSheet $event) {     
			    $event->sheet->autoSize();
			
                $event->sheet->getDelegate()->getStyle('A1:A1')->getFont()->setSize(14);
			    
			    $event->sheet->setAutoFilter('A5:Q5');
			    
			    $event->sheet->getStyle('A5:Q5')->getFill()
							 ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('8db4e2'); 						   			    
            },		
        ];
    }
} 