<?php
namespace App\Exports;

use App\Member;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class MembersExport implements FromView, WithEvents
{
    public function view(): View
    {
		$members = Member::with("type")->with("qualifications")->with("awards")->get();
	
        return view('admin/members/export-members', [
            'members' => $members
        ]);
    }
	
	public function registerEvents(): array
    {		
        return [
			AfterSheet::class    => function(AfterSheet $event) {     
			    $event->sheet->autoSize();			   
			
                $event->sheet->getDelegate()->getStyle('A1:A1')->getFont()->setSize(14);
			    
			    $event->sheet->setAutoFilter('A5:AF5');
			    
			    $event->sheet->getStyle('A5:AF5')->getFill()
							 ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
							 ->getStartColor()->setARGB('8db4e2'); 						   			    
            },		
        ];
    }
} 