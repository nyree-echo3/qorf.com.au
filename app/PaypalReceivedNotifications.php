<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalReceivedNotifications extends Model
{
    protected $table = 'paypal_received_notifications';
}
