<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class PropertyCategory extends Model
{
    use SortableTrait;

    protected $table = 'property_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'category_id');
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','properties')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'properties/'.$this->attributes['slug'];
    }
}
